window.addEventListener('loadedCard', function (e){
  /*var elements = document.querySelectorAll(".card-level");
  for(var i in elements){
    elements[i].addEventListener('loadedCard', function(e){
      console.log(e);
    })
    // var objPosition = localStorage.getItem(elements[i].id);
    // if(objPosition != null) setPositionElement(elements[i].id, JSON.parse(objPosition));
    // dragElement(elements[i]);
  }
  */
})

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement(e) {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
    var idParent = e.target.id.replace('header', '');
    var elementParent = document.getElementById(idParent);
    localStorage.setItem(idParent, JSON.stringify({x : elementParent.offsetLeft, y: elementParent.offsetTop}));
  }
}

function setPositionElement(elementid, objposition){
  if(objposition){
    elementid = elementid.replace('header', '');
    var element = document.getElementById(elementid);
    element.style.top = objposition.y+'px';
    element.style.left = objposition.x+"px";
  }
}