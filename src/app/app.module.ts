import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './Components/Main/header/header.component';
import { NavMenuComponent } from './Components/Main/nav-menu/nav-menu.component';
import { MainComponent } from './Components/Main/main/main.component';
import { FooterComponent } from './Components/Main/footer/footer.component';

import { TreeTableModule } from 'primeng/treetable';
import { BadgeModule } from 'primeng/badge';
import { TableModule } from 'primeng/table';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { ChartModule } from 'primeng/chart';
import { DropdownModule } from 'primeng/dropdown';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DialogModule } from 'primeng/dialog';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { TooltipModule } from 'primeng/tooltip';
import { ToastModule } from 'primeng/toast';
import { SidebarModule } from 'primeng/sidebar';
import { ListboxModule } from 'primeng/listbox';
import { PanelModule } from 'primeng/panel';
import { CheckboxModule } from 'primeng/checkbox';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { GalleriaModule } from 'primeng/galleria';
import { DataViewModule } from 'primeng/dataview';
import { InputTextModule } from 'primeng/inputtext';
import { MenuModule } from 'primeng/menu';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { ptBrLocale } from 'ngx-bootstrap/locale';

import { NgxEchartsModule } from 'ngx-echarts';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from './Core/Services/config.service';
import { catchError, map } from 'rxjs/operators';
import { Observable, ObservableInput, of } from 'rxjs';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { SpinnerComponent } from './Components/Main/spinner/spinner.component';
import { MessageService } from 'primeng/api';
import { SideMenuComponent } from './Components/Equipments/Panel/side-menu/side-menu.component';
import { TableAlarmsComponent } from './Components/Equipments/Panel/table-alarms/table-alarms.component';
import { ImagePanelComponent } from './Components/Equipments/Panel/image-panel/image-panel.component';
import { CardPlantLevelComponent } from './Components/Equipments/Panel/card-plant-level/card-plant-level.component';
import { PlantLevelDetailsComponent } from './Components/Equipments/Panel/plant-level-details/plant-level-details.component';
import { PanelEquipmentsComponent } from './Components/Equipments/Panel/panel-equipments/panel-equipments.component';
import { EquipmentComponent } from './Components/Equipments/Equipment/equipment/equipment.component';
import { RouterModule, ROUTES } from '@angular/router';
import { TableVariablesComponent } from './Components/Equipments/Equipment/table-variables/table-variables.component';
import { CardIndicatorComponent } from './Components/Equipments/Equipment/card-indicator/card-indicator.component';
import { SearchBarComponent } from './Components/Main/search-bar/search-bar.component';
import { ConfigurationsComponent } from './Components/configurations/configurations.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { ReportEquipmentsComponent } from './Components/Reports/report-equipments/report-equipments.component';
import { ReportPdfComponent } from './Components/Reports/report-pdf/report-pdf.component';
import { LoginComponent } from './Components/Main/login/login.component';


defineLocale('pt-br', ptBrLocale);
function load(http: HttpClient, config: ConfigService): (() => Promise<boolean>) {
  return (): Promise<boolean> => {
    return new Promise<boolean>((resolve: (a: boolean) => void): void => {
       http.get('assets/config.json')
         .pipe(
           map((x: ConfigService) => {
             config.configs = x;
             config.baseURL = x.baseURL;
             resolve(true);
           }),
           catchError((x: { status: number }, caught: Observable<void>): ObservableInput<{}> => {
             if (x.status !== 404) {
               resolve(false);
             }
             config.baseURL = 'http://localhost:8080/api';
             resolve(true);
             return of({});
           })
         ).subscribe();
    });
  };
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavMenuComponent,
    MainComponent,
    SideMenuComponent,
    TableAlarmsComponent,
    ImagePanelComponent,
    CardPlantLevelComponent,
    SpinnerComponent,
    PlantLevelDetailsComponent,
    FooterComponent,
    PanelEquipmentsComponent,
    EquipmentComponent,
    TableVariablesComponent,
    CardIndicatorComponent,
    SearchBarComponent,
    ConfigurationsComponent,
    DashboardComponent,
    ReportEquipmentsComponent,
    ReportPdfComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TreeTableModule,
    TableModule,
    BadgeModule,
    CalendarModule,
    CardModule,
    DropdownModule,
    FontAwesomeModule,
    DragDropModule,
    BreadcrumbModule,
    DialogModule,
    ChartModule,
    ToastModule,
    AutoCompleteModule,
    TooltipModule,
    SidebarModule,
    ListboxModule,
    PanelModule,
    CheckboxModule,
    InputNumberModule,
    InputMaskModule,
    InputTextModule,
    AppRoutingModule,
    ConfirmDialogModule,
    GalleriaModule,
    DataViewModule,
    MenuModule,
    BsDatepickerModule.forRoot(),
    NgxEchartsModule.forRoot({
      echarts: ()=>import('echarts')
    }),
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
    })
  ],
  providers: [
    MessageService,
    ConfirmationService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps:[HttpClient, ConfigService],
      useFactory: load
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
