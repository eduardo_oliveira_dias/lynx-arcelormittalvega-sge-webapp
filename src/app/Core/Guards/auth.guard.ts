import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorizeService } from '../Services/authorize.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor(private router: Router, private authorizeService: AuthorizeService) {
    
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree 
  {
    var token = localStorage.getItem("tokenSGE");
    var expiration = localStorage.getItem("expirationToken");
    var dateExpiration = new Date(expiration);
    var dateNow = new Date();
    
    if((token != null && token != '') && dateExpiration > dateNow){
      this.authorizeService.showMenuEvent.emit(true);
      return true;
    }else{
      this.authorizeService.showMenuEvent.emit(false);
      this.router.navigate(["/login"])
    }
  }
  
}
