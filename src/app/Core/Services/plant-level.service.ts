import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { PlantLevel } from '../Models/PlantLevel';

@Injectable({
  providedIn: 'root'
})
export class PlantLevelService {
  apiURL: string;
  options = {};

  constructor(private configService:ConfigService, private http: HttpClient) { 
    this.apiURL = configService.baseURL;
    this.options = this.configService.options;
  }

  getPlantLevels(){
    return this.http.get(this.apiURL+"api/PlantLevels", this.options)
      .toPromise()
      .then(res => <PlantLevel> res)
      .then(plantLevel => plantLevel);
  }

  getPlantLevelsByQuery(query: string){
    return this.http.get(this.apiURL+"api/PlantLevels/"+query, this.options)
      .toPromise()
      .then(res => <PlantLevel[]> res)
      .then(plantLevel => plantLevel);
  }

  getPlantLevelsByLevelName(levelName:string, parentName: string = ""){
    return this.http.get(this.apiURL+"api/PlantLevels/level/"+levelName+"/"+parentName, this.options)
    .toPromise()
    .then(res => <PlantLevel[]> res)
    .then(plantLevel => plantLevel);
  }

}
