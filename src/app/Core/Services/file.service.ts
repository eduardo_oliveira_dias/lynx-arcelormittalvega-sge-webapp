import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Image } from '../Models/Image';
import ImageResult from '../Models/Result/ImageResult';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  apiUrl: string;
  options = {};
  optionsFile = {};
  constructor(private http: HttpClient, private configService: ConfigService) { 
    this.apiUrl = this.configService.baseURL;
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')
        })
      }
    this.optionsFile = {
      headers: new HttpHeaders({
        'Content-Type':  'multipart/form-data',
        
        })
      }
  }

  uploadImage(file: File, name: string){
    const formData = new FormData();
    formData.append('file', file);
    formData.append('name',name);
    var request = new HttpRequest('POST',this.apiUrl+"api/images", formData,{
      headers:new HttpHeaders({'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')})
    });
    //request.headers = request.headers.append('Authorization','Bearer '+localStorage.getItem('tokenSGE'));
    return this.http.request<any>(request)
    .toPromise()
    .then(res => res)
    
  }

  getImage(name:string){
    return this.http.get(this.apiUrl+"api/images/"+name, this.options)
      .toPromise()
      .then(res => <ImageResult>res)
      .then(data => data);
  }

  getSavedImages(){
    return this.http.get(this.apiUrl+"api/images/", this.options)
      .toPromise()
      .then(res => <Image[]>res)
      .then(data=>data)
  }

  deleteImage(imageName: string){
    return this.http.delete(this.apiUrl+"api/images/"+imageName, this.options)
      .toPromise()
      .then(res => res)
  }
}
