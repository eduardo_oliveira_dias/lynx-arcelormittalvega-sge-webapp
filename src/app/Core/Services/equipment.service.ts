import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Equipment } from '../Models/Equipment';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class EquipmentService {
  apiURL: string;
  options = {};
  
  constructor(private configService: ConfigService, private http: HttpClient) { 
    this.apiURL = this.configService.baseURL;
    this.options = this.configService.options;
  }

  getEquipment(equipmentName: string){
    return this.http.get(this.apiURL+"api/equipments/"+equipmentName, this.options)
      .toPromise()
      .then(res => <Equipment>res)
      .then(data => data);
  }

}
