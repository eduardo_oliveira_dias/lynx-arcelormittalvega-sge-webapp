import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { UserLogin } from '../Models/UserLogin';
import { ConfigService } from './config.service';
import { httpOptions } from '../Models/util/httpOptions';
import { UserToken } from '../Models/UserToken';

@Injectable({
  providedIn: 'root'
})
export class AuthorizeService {
  apiUrl: string;
  options = {};
  showMenuEvent= new EventEmitter<boolean>();

  constructor(private configService: ConfigService, private http: HttpClient) { 
    this.apiUrl = this.configService.baseURL;
    this.options = this.configService.options;
  }

  login(user: UserLogin){
    return this.http.post(this.apiUrl+"api/authorize/login", user, this.options)
      .toPromise()
      .then(res => <UserToken>res)
      .then(data => data)
  }

  validateUser(){
    return this.http.get(this.apiUrl+"api/authorize", this.options)
      .toPromise()
      .then(res => res)
      .then(data => data)
  }
}
