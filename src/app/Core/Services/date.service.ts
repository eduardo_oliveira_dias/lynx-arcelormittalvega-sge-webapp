import { Injectable } from '@angular/core';
import {DateTime} from 'luxon';
@Injectable({
  providedIn: 'root'
})
export class DateService {

  constructor() { }

  addDays(date: Date, days: number){
    date.setDate(date.getDate() + (days));

    return date;
  }

  getToday(){
    var dateToday = new Date();
    return new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, dateToday.getDate(),0,0,0).ts);
  }

  getDateFromIso(date:string) : Date{
    var dateConverted = DateTime.fromISO(date);
    return dateConverted.toJSDate();
  }

  splitDateTimeInMS(startDate:string, endDate: string) : number {
    var startTs =  DateTime.fromISO(startDate).ts;
    var endTs =  DateTime.fromISO(endDate).ts;
    var splitTs = startTs + ((endTs - startTs) /2);

    return splitTs;
  }

  add(timeMeasure: 'seconds'|'minutes'|'hours'|'days'|'months'|'years', date: Date, value:number){
    let dateTime = DateTime.local(date.getFullYear(), date.getMonth()+1, date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
    let obj = {
      [timeMeasure]:value
    };
   
    let dateTimeCalculated = new Date(dateTime.plus(obj).ts);

    return dateTimeCalculated;
  }
}