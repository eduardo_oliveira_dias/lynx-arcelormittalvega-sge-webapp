import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReportEquipments } from '../Models/Report/ReportEquipments';
import { ReportFilter } from '../Models/Request/ReportFilter';
import { httpOptions } from '../Models/util/httpOptions';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  apiUrl: string
  options = {};
  
  constructor(private config: ConfigService, private http: HttpClient) { 
    this.apiUrl = this.config.baseURL;
    this.options = this.config.options;
  }

  getReportEquipments(reportFilter: ReportFilter){
    return this.http.post(this.apiUrl+"api/report/equipments", reportFilter, this.options)
      .toPromise()
      .then(res => <ReportEquipments> res)
      .then(data => data);
  }
}
