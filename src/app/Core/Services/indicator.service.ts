import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Indicator } from '../Models/Indicator';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class IndicatorService {
  apiURL: string;
  options = {};
  constructor(private configService: ConfigService, private http: HttpClient) { 
    this.apiURL = this.configService.baseURL;
    this.options = this.configService.options;
  }

  getKPI(itemName: string){
    return this.http.get(this.apiURL+"api/Indicators/kpi/"+itemName, this.options)
      .toPromise()
      .then(res => <Indicator>res)
      .then(data => data);
  }
}
