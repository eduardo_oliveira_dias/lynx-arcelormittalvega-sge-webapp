import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SystemConfiguration } from '../Models/SystemConfiguration';
import { RequestUpdateConfigurations } from '../Models/Request/RequestUpdateConfigurations';
import { httpOptions } from '../Models/util/httpOptions';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  baseURL : string;
  configs: {};
  options = httpOptions;
  constructor(private http : HttpClient) { 
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')
        })
      }
  }

  get appConfig(){
    if(!this.configs){
      throw Error("Erro ao carregar as configurações.");
    }
    return this.configs;
  }

  getGeneralConfigs(){
    return this.http.get(this.baseURL+"api/configurations/general", this.options)
          .toPromise()
          .then(res => <SystemConfiguration[]>res)
          .then(data => data)
  }

  getItemPositions(){
    return this.http.get(this.baseURL+"api/configurations/itemPositions", this.options)
          .toPromise()
          .then(res => <SystemConfiguration[]>res)
          .then(data =>data)
  }

  getVariableParameters(){
    return this.http.get(this.baseURL+"api/configurations/variableParameters", this.options)
          .toPromise()
          .then(res => <SystemConfiguration[]>res)
          .then(data =>data)
  }

  updateGeneralConfiguration(request: RequestUpdateConfigurations){
    return this.http.put(this.baseURL+"api/configurations/general", request, this.options)
      .toPromise()
      .then(res => res);
  }

  deleteVariableParameters(key: string){
    return this.http.delete(this.baseURL+"api/configurations/variableParameters/"+key, this.options)
      .toPromise()
      .then(res => res);
  }

  insertVariableParameters(parameter: SystemConfiguration){
    return this.http.post(this.baseURL+"api/configurations/variableParameters", parameter, this.options)
      .toPromise()
      .then(res => res);
  }

  updateOptions(){
    this.options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')
        })
      }
  }
}
