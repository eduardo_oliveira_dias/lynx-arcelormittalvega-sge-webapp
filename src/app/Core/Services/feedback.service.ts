import { Component, Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})

export class FeedbackService {

  constructor(private messageService:MessageService) { }

  success(msg: string){
    this.messageService.add({key:'toastMessage',severity: 'success', summary:"Sucesso!",detail:msg})
  }
  error(msg: string){
    this.messageService.add({key:'toastMessage',severity: 'error', summary:"Erro",detail:msg})
  }
  info(msg: string){
    this.messageService.add({key:'toastMessage',severity: 'info', summary:"Info",detail:msg})
  }
  warn(msg: string){
    this.messageService.add({key:'toastMessage',severity: 'warn', summary:"Atenção",detail:msg})
  }
}