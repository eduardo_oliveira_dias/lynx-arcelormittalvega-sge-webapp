import { TestBed } from '@angular/core/testing';

import { PlantLevelService } from './plant-level.service';

describe('PlantLevelService', () => {
  let service: PlantLevelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlantLevelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
