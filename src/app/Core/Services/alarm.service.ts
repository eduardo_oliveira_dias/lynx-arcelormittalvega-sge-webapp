import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Alarm } from '../Models/Alarm';
import { AlarmRequest } from '../Models/Request/AlarmRequest';
import { httpOptions } from '../Models/util/httpOptions';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class AlarmService {
  apiURL: string;
  options = {}

  constructor(private configService: ConfigService, private http: HttpClient) { 
    this.apiURL = configService.baseURL;
    this.options = this.configService.options;
    
  }

  getCriticalAlarms(){
    return this.http.get(this.apiURL+'api/Alarms/critical', this.options)
      .toPromise()
      .then(res => <Alarm[]> res)
      .then(alarms => alarms);
  }

  getAlarmsByName(level: string, name: string){
    return this.http.get(this.apiURL+'api/Alarms/'+level+"/"+name,this.options)
      .toPromise()
      .then(res => <Alarm[]> res)
      .then(alarms => alarms);
  }
  getTrendAlarms(alarmRequest: AlarmRequest){

    return this.http.post(this.apiURL+"api/alarms/trend", alarmRequest, this.options)
      .toPromise()
      .then(res => <Alarm[]>res)
      .then(data => data)
  }
}
