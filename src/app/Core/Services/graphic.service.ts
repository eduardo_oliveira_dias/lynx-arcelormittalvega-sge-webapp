import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { GraphicSector } from '../Models/Graphic/GraphicSector';
import { GraphicTrendRequest } from '../Models/Request/GraphicTrendRequest';
import { httpOptions } from '../Models/util/httpOptions';
import { GraphicTrend } from '../Models/Graphic/GraphicTrend';
import { RequestGraphicSector } from '../Models/Request/RequestGraphicSector';
import { GraphicBar } from '../Models/Graphic/GraphicBar';
import { RequestGraphicBar } from '../Models/Request/RequestGraphicBar';
import { GraphicResult } from '../Models/Result/GraphicResult';

@Injectable({
  providedIn: 'root'
})
export class GraphicService {
  apiUrl:string;
  options = {};

  constructor(private configService: ConfigService, private http: HttpClient) { 
    this.apiUrl = configService.baseURL;
    this.options = this.configService.options;
  }

  getGraphicSector(childrenLevel:string, parentLevel: string, parentLevelName: string = "BV"){
    return this.http.get(this.apiUrl+"api/graphics/sector/"+childrenLevel+"/"+parentLevel+"/"+parentLevelName, this.options)
              .toPromise()
              .then(res => <GraphicSector[]> res)
              .then(data => data);

  }

  getTrendAlarms(alarmRequest: GraphicTrendRequest){
    return this.http.post(this.apiUrl+"api/graphics/getTrend", alarmRequest, this.options)
      .toPromise()
      .then(res => <GraphicTrend[]>res)
      .then(data => data)
  }

  getAlarmsByType(request: RequestGraphicSector){
    return this.http.post(this.apiUrl+"api/graphics", request, this.options)
      .toPromise()
      .then(res => <GraphicResult<GraphicSector>>res)
      .then(data => data)
  }

  mountAlarmsByType(request: RequestGraphicSector){
    return this.http.post(this.apiUrl+"api/graphics/mountType", request, this.options)
      .toPromise()
      .then(res => <GraphicResult<GraphicSector>>res)
      .then(data => data)
  }

  getAlarmsByGroup(request: RequestGraphicBar){
    return this.http.post(this.apiUrl+"api/graphics/groupBar", request, this.options)
      .toPromise()
      .then(res => <GraphicResult<GraphicBar[]>>res)
      .then(data => data)
  }

  mountAlarmsByGroup(request: RequestGraphicBar){
    return this.http.post(this.apiUrl+"api/graphics/mountBar", request, this.options)
      .toPromise()
      .then(res => <GraphicResult<GraphicBar[]>>res)
      .then(data => data)
  }
}
