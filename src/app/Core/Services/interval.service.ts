import { Injectable } from '@angular/core';
import {DateTime} from 'luxon';

@Injectable({
  providedIn: 'root'
})
export class IntervalService {

  constructor() { }

  getIntervals(){
    return [
      {label: 'Mês Passado', value: 1},
      {label: 'Mês Atual', value: 2},
      {label: 'Dia Anterior', value: 3},
      {label: 'Dia Atual', value: 4},
      {label: 'Ultimos Sete Dias', value: 5},
      {label: 'Ultimos Trinta Dias', value: 6},
    ]
  }

  addDays(date: Date, days: number){
    date.setDate(date.getDate() + (days));

    return date;
  }

  setInterval(intervalValue: number){
    var Dates = { inicio: new Date(), fim: new Date() };
    var dateToday = new Date();
    switch(intervalValue){
      case 1:
        Dates.fim = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, dateToday.getDate(),23,59,59).minus({month: 1}).endOf('month').ts);
        Dates.inicio = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, 1, 0, 0, 0).minus({month: 1}).ts);
        break;
      case 2:
        Dates.inicio = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, 1, 0, 0, 0).ts);
        break;
      case 3:
        Dates.fim = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, dateToday.getDate(),23,59,59).minus({days: 1}).ts);
        Dates.inicio = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, dateToday.getDate(),0,0,0).minus({days: 1}).ts);
        break;
      case 4:
        Dates.fim = new Date();
        Dates.inicio = new Date(DateTime.local(dateToday.getFullYear(), dateToday.getMonth()+1, dateToday.getDate(),0,0,0).ts);
        break;
      case 5:
        Dates.inicio = new Date(DateTime.local().minus({days: 7}).ts);
        break;
      case 6:
        Dates.inicio = new Date(DateTime.local().minus({days: 30}).ts);
        break;
    }

    return Dates;
  }
}
