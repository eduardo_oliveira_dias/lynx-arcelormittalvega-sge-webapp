import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VariableParameterRequest } from '../Models/Request/VariableParameterRequest';
import { TagVariable } from '../Models/TagVariable';
import { httpOptions } from '../Models/util/httpOptions';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class VariableService {
  apiUrl: string;
  options = {};
  
  constructor(private http: HttpClient, private configService: ConfigService) { 
    this.apiUrl = this.configService.baseURL;
    this.options = this.configService.options;
  }

  getVariables(name: string){
    return this.http.get(this.apiUrl+"api/variables/"+name, this.options)
      .toPromise()
      .then(res => <TagVariable[]>res)
      .then(variable => variable);
  }
  updateParameter(request: VariableParameterRequest){
    return this.http.post(this.apiUrl+"api/variables/parameter", request, this.options)
      .toPromise()
      .then(res => res)
  }
}
