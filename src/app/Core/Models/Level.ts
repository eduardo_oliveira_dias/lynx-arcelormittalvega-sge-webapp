export enum Level{
    Plant = 1,
    Segment = 2,
    Area = 3,
    Section = 4,
    Equipment = 5
}