import { Alarm } from "../Alarm";

export class GraphicResult<T>{
    graphicData: T;
    alarms: Alarm[];
}