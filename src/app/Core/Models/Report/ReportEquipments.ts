import { Equipment } from "../Equipment";
import { GraphicBar } from "../Graphic/GraphicBar";
import { GraphicTrend } from "../Graphic/GraphicTrend";

export class ReportEquipments{
    graphicEquipmentStatus: GraphicBar[];
    graphicKPIs: GraphicTrend[];
    equipmentsStatus: Equipment[];
}