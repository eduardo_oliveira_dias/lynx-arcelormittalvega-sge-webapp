export class CardLevel{
    name: string;
    level: string;
    description: string;
    kpi: any;
    chartData: [];
}