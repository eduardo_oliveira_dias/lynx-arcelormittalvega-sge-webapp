import { GraphicDataItem } from "./GraphicDataItem";

export class GraphicData{
    info: string;
    description: string;
    items: GraphicDataItem[];
}