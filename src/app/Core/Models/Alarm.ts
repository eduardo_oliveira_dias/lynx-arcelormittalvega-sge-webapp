import { timestamp } from "rxjs/operators"

export interface Alarm{
    variableName: string;
    variableDescription: string;
    engUnit: string;
    value: any;
    timeStamp: Date;
    equipmentName: string;
    equipmentDescription: string;
    areaName: string;
    areaDescription: string;
    severity: string;

    
}