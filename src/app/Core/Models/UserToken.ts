import { UserAD } from "./UserAD";

export class UserToken{
    authenticated: boolean;
    token: string;
    message: string;
    expiration: Date;
    userLogged: UserAD;
}