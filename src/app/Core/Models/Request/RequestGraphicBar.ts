import { Alarm } from "../Alarm";
import { Level } from "../Level";

export class RequestGraphicBar{
    startTime: Date;
    endTime: Date;
    level: Level;
    levelName: string;
    alarms: Alarm[];
}