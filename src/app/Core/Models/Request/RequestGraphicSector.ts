import { Alarm } from "../Alarm";
import { Level } from "../Level";

export class RequestGraphicSector{
    startTime: Date;
    endTime: Date;
    level: Level;
    levelName: string;
    alarms: Alarm[];
}