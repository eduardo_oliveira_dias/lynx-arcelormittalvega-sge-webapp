import { Level } from "../Level";

export class AlarmRequest{
    variableName: string;
    startTime: Date;
    endTime: Date;
    level: string;
    levelName: string;
}