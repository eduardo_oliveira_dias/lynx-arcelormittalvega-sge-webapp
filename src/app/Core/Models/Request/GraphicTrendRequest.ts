export class GraphicTrendRequest{
    variableNames: string[];
    startTime: Date;
    endTime: Date;
}