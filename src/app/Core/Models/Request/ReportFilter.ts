export class ReportFilter{
    startTime: Date;
    endTime: Date;
    segmentName: string;
    areaName: string;
    sectionName: string;
    equipmentName: string;
}