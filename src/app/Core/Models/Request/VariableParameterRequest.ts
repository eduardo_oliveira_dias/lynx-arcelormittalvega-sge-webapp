import { Variable } from "@angular/compiler/src/render3/r3_ast";

import { TagVariable } from "../TagVariable";
export class VariableParameterRequest{
    name: string;
    field: string;
    newValue: any;
    value: any;
    variable: TagVariable;
}