export interface TagVariable{
    name: string;
    description: string;
    value: any;
    equipmentName: string;
    equipmentDescription: string;
    severity:string
}