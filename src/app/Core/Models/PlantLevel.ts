export class PlantLevel{
    name: string;
    description: string;
    level: number;
    parent: string;
    children: PlantLevel[];
    alarmCount: number;
    totalAlarms: number;
    parentName: string;
}