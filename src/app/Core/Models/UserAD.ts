export class UserAD{
    displayName: string;
    samAccountName:string;
    name: string;
    middleName:string;
    userPrincipalName: string;
    distinguinshedName:string;
    roles: string[];
}