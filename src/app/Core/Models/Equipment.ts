import { TagVariable } from "./TagVariable";

export class Equipment{
    name: string;
    description: string;
    variables: TagVariable[];
    segmentName: string;
    segmentDescription: string;
    areaName: string;
    areaDescription: string;
    sectionName: string;
    sectionDescription: string;
    severityStatus: string;
    timeStampSeverityStatus: Date;
    criticalStatusPeriod: number;
    alertStatusPeriod: number;
    severityStatusDurationView: string;
}