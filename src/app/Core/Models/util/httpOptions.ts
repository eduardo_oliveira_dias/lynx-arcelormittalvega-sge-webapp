import { HttpHeaders } from "@angular/common/http";

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')
    })
  };
var httpOptionsFile = {
    headers: new HttpHeaders({
      'Content-Type':  'multipart/form-data',
      'Authorization': 'Bearer '+localStorage.getItem('tokenSGE')
    })
  };

export {httpOptions};
export {httpOptionsFile};