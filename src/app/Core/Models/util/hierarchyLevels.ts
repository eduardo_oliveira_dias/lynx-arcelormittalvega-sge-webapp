export const hierarchyLevels = [
    { name: "root", children: "segmentName" },
    { name: "segmentName", children: "areaName" },
    { name: "areaName", children: "sectionName" },
    { name: "sectionName", children: "equipmentName" }
  ];