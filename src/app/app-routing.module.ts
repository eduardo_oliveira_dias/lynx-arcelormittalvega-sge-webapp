import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationsComponent } from './Components/configurations/configurations.component';
import { DashboardComponent } from './Components/dashboard/dashboard.component';
import { EquipmentComponent } from './Components/Equipments/Equipment/equipment/equipment.component';
import { PanelEquipmentsComponent } from './Components/Equipments/Panel/panel-equipments/panel-equipments.component';
import { LoginComponent } from './Components/Main/login/login.component';
import { ReportEquipmentsComponent } from './Components/Reports/report-equipments/report-equipments.component';
import { AuthGuard } from './Core/Guards/auth.guard';
import { RouteResolver } from './Core/Resolvers/RouteResolver';

const routes: Routes = [
  {path:"", component: PanelEquipmentsComponent, pathMatch:'full'},
  {path:"equipment/:code", component:EquipmentComponent, resolve:RouteResolver},
  {path:"configurations", component: ConfigurationsComponent},
  {path:"dashboard", component: DashboardComponent},
  {path:"report-equipments", component: ReportEquipmentsComponent},
  {path:'login', component:LoginComponent}
].map(x => {
  if(x.path != 'login'){
    let arrayGuards = [];
    return {...x, canActivate:[AuthGuard]}
  }else{
    return x;
  }
});

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'}),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
