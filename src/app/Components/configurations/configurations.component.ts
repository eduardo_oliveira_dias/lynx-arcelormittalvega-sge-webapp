import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { faUserCog, faSave, faTrash, faPlus, faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
import { ConfirmationService } from 'primeng/api';
import { Equipment } from 'src/app/Core/Models/Equipment';
import { Image } from 'src/app/Core/Models/Image';
import { PlantLevel } from 'src/app/Core/Models/PlantLevel';
import { RequestUpdateConfigurations } from 'src/app/Core/Models/Request/RequestUpdateConfigurations';
import { SystemConfiguration } from 'src/app/Core/Models/SystemConfiguration';
import { ConfigService } from 'src/app/Core/Services/config.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
import { FileService } from 'src/app/Core/Services/file.service';
import { PlantLevelService } from 'src/app/Core/Services/plant-level.service';

@Component({
  selector: 'app-configurations',
  templateUrl: './configurations.component.html',
  styleUrls: ['./configurations.component.css']
})
export class ConfigurationsComponent implements OnInit {
  faUserCog = faUserCog;
  faSave = faSave;
  faTrash = faTrash;
  faPlus = faPlus;
  faTimes = faTimes;
  faCheck = faCheck;
  generalConfigurations= {
    OnlyActiveVariables: false,
    ValueRefreshTime: '',
    GraphicTimeIntervalSearch: 0,
  };
  variableParameters: SystemConfiguration[];
  groupOptions: ConfigurationOptions[] = [
    {name: "Equipamentos", code:"equipments", inactive:false},
    {name: "Imagens", code:"images", inactive:false},
    {name: "Usuários e permissões", code: "manageUsers", inactive:true},
    {name: "Relatórios", code:"reports", inactive:true}
  ]
  selectedGroup : ConfigurationOptions;

  formGeneralConfigs: FormGroup;
  formInsertParameter: FormGroup;
  parameterInsert = new SystemConfiguration();

  displayModalInsert = false;
  savingParameter = false;

  images: Image[] = [];
  pathImages = '';
  displayCustom: boolean = false;

  activeIndex: number = 0;
  responsiveOptions:any[] = [
    {
        breakpoint: '1024px',
        numVisible: 5
    },
    {
        breakpoint: '768px',
        numVisible: 3
    },
    {
        breakpoint: '560px',
        numVisible: 1
    }
  ];

  imageFile: File;
  displayModalFile = false;
  savingImage = false;
  plantLevel: PlantLevel = new PlantLevel();
  plantLevels: PlantLevel[] = [];

  constructor(private configService: ConfigService, 
      private feedBackService: FeedbackService,
      private confirmationService: ConfirmationService,
      private fileService: FileService,
      private plantLevelService: PlantLevelService) 
  { 
        this.selectedGroup = this.groupOptions[0];

        this.formGeneralConfigs = new FormGroup({
          OnlyActiveVariables: new FormControl(false, [Validators.required]),
          ValueRefreshTime: new FormControl('', [Validators.required]),
          GraphicTimeIntervalSearch: new FormControl('', [Validators.required])
        })
  }

  ngOnInit(): void {
    this.formInsertParameter = new FormGroup({
      key: new FormControl('', [Validators.required]),
      value: new FormControl('', [Validators.required])
    });
    this.searchEquipments('bv');
    this.pathImages = this.configService.configs['pathImages'];
    // this.configService.getVariableParameters()
    //   .then(res =>{
    //     this.variableParameters = res;
    //   })
    //   .catch(err =>{
    //     this.feedBackService.error(err.error);
    //   })
    
    this.configService.getGeneralConfigs()
      .then(res =>{
        for(var config of res){
          this.generalConfigurations[config.key] = config.value;
        }

      })
      .catch(err =>{
        this.feedBackService.error(err.error);
      });

      this.getSavedImages();
  }

  saveGeneralConfigs(){
    if(this.formGeneralConfigs.valid){
      var request = new RequestUpdateConfigurations();
      request.configurations = [];
      for(var config of Object.keys(this.formGeneralConfigs.value)){
        let configuration = new SystemConfiguration();
        configuration.key = config;
        configuration.value = this.formGeneralConfigs.value[config].toString();
        request.configurations.push(configuration);
      }

      this.configService.updateGeneralConfiguration(request)
        .then(res => {
          this.feedBackService.success(res['message']);
        })
        .catch(err =>{
          this.feedBackService.error(err.error);
        })

    }
  }

  removeParameter(parameter: SystemConfiguration){
    this.confirmationService.confirm({
      message: "Tem certeza que deseja remover o parâmetro: "+parameter.key+"?",
      accept: () =>{
        this.configService.deleteVariableParameters(parameter.key)
          .then(res => {
            this.feedBackService.success(res['message']);
            this.variableParameters =this.variableParameters.filter(x => x.key != parameter.key);
          })
          .catch(err =>{
            this.feedBackService.error(err.error);
          })
      }
    });
  }

  confirmDeleteImage(imageName: string){
    this.confirmationService.confirm({
      message: "Tem certeza que deseja remover a imagem de "+imageName+"?",
      accept: () =>{
        this.fileService.deleteImage(imageName)
          .then(res => {
            this.feedBackService.success(res['message']);
            this.images =this.images.filter(x => x.name != imageName);
          })
          .catch(err =>{
            this.feedBackService.error(err.error);
          })
      }
    });
  }

  insertParameter(){
    if(this.formInsertParameter.valid){
      this.savingParameter = true;
      this.configService.insertVariableParameters(this.parameterInsert)
        .then(res =>{
          this.feedBackService.success(res['message']);
        })
        .catch(err => {
          this.feedBackService.error(err.error);
        })
        .finally(()=>{
          this.savingParameter = false;
          this.displayModalInsert = false;
          this.parameterInsert = new SystemConfiguration();
          this.formInsertParameter.reset();
        })
    }
  }

  imageClick(index: number) {
    this.activeIndex = index;
    this.displayCustom = true;
  }

  onChange(event){
    var file = event.srcElement.files[0];
    if(file != null){
      this.imageFile = file; 
  
      document.getElementById('image-file-label').innerHTML = file.name;
    }
  }

  resetInput(){
    
    try{
      var inputFile = document.getElementById('image-file-label');
      if(inputFile) inputFile.innerHTML = 'Escolher arquivo';

    }catch(e){
      console.log(e);
    }
  }

  saveFile(){
    console.log(this.plantLevel.name);
    if(this.imageFile != null && this.plantLevel.name != undefined){
      this.savingImage = true;
      this.fileService.uploadImage(this.imageFile,this.plantLevel.name)
        .then(res => {
          this.displayModalFile = false;
          this.savingImage = false;
          this.feedBackService.success(res['body'].message);
          this.getSavedImages();
        }).catch(err =>{
          this.feedBackService.error(err.error);
          this.savingImage = false;
        });
    }
  }

  searchEquipments(value){
    this.plantLevelService.getPlantLevelsByQuery(value)
      .then(res => {
        this.plantLevels = res;
      })
      .catch(err =>{
        this.feedBackService.error(err.error);
      });
  }

  getSavedImages(){
    this.images = [];
    this.fileService.getSavedImages()
      .then(res=>{
        this.images = res;
      })
      .catch(err=>{
        this.feedBackService.error(err.error);
      })
  }

  get parameterKey(){
    return this.formInsertParameter.get('key');
  }

  get parameterValue(){
    return this.formInsertParameter.get('value');
  }
}
interface ConfigurationOptions{
  name: string;
  code: string;
  inactive: boolean;
}
