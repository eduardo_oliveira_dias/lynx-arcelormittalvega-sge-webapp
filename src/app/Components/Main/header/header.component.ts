import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faCaretDown, faUser } from '@fortawesome/free-solid-svg-icons';
import { MenuItem } from 'primeng/api/menuitem';
import { UserAD } from 'src/app/Core/Models/UserAD';
import { AuthorizeService } from 'src/app/Core/Services/authorize.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  faUser = faUser;
  faCaretDown = faCaretDown;
  items: MenuItem[];
  userLogged: UserAD = new UserAD();
  constructor(private authorizeService: AuthorizeService, private router: Router) {
    this.items = [{
      label: 'Logout',
      command: () => this.logout()
    }]
   }

  ngOnInit(): void {
    this.userLogged = <UserAD>JSON.parse(localStorage.getItem("userLogged"));
  }

  logout(){
    localStorage.removeItem("tokenSGE");
    localStorage.removeItem("expirationToken");
    localStorage.removeItem("userLogged");
    this.authorizeService.showMenuEvent.emit(false);
    this.router.navigate(['/login'])
  }
}
