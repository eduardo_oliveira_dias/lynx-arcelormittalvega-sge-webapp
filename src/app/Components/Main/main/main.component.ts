import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { PrimeNGConfig } from 'primeng/api';
import { AuthorizeService } from 'src/app/Core/Services/authorize.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  showHeader= false;
  showFooter= false;
  constructor(private config: PrimeNGConfig, 
    private translateService: TranslateService,
    private authorizeService: AuthorizeService) { 
      this.translate('ptbr');
    }

  ngOnInit(): void {
    this.authorizeService.showMenuEvent.subscribe(show=>{
      this.showHeader = show;
      this.showFooter = show;
    })
  }
  translate(lang: string) {
    this.translateService.use(lang);
    this.translateService.get('primeng').subscribe(res => this.config.setTranslation(res));
  }
}
