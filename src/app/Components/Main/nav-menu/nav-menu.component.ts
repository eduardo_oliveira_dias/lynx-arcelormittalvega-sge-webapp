import { Component, OnInit } from '@angular/core';
import { faTachometerAlt, faCogs, faFileAlt, faSearch, faUserCog } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  faTachometerAlt = faTachometerAlt;
  faCogs = faCogs;
  faFileAlt = faFileAlt;
  faSearch = faSearch;
  faUserCog = faUserCog;
  displaySearch = false;
  constructor() { }

  ngOnInit(): void {
  }

  showSearchBar(){
    console.log(this.displaySearch);
    this.displaySearch = true;
  }
  hideBar(eventValue){
    this.displaySearch = eventValue;
  }
}
