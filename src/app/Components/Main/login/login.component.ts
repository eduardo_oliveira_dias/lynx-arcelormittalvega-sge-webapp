import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faLock, faUser, faSignInAlt, faSpinner, faPray } from '@fortawesome/free-solid-svg-icons';
import { UserLogin } from 'src/app/Core/Models/UserLogin';
import { AuthorizeService } from 'src/app/Core/Services/authorize.service';
import { ConfigService } from 'src/app/Core/Services/config.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  faUser= faUser;
  faLock = faLock;
  faSignInAlt = faSignInAlt;
  faSpinner = faSpinner;
  loading = false;
  formLogin: FormGroup;
  userLogin = new UserLogin();

  constructor(private authorizeService: AuthorizeService,private configService: ConfigService,
      private feedbackService: FeedbackService, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = new FormGroup({
      name: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
    //this.isLogged();
  }

  login(){
    if(this.formLogin.valid){
      this.loading = true;
      this.authorizeService.login(this.userLogin)
        .then(res => {
          localStorage.setItem("tokenSGE", res.token);
          localStorage.setItem("expirationToken", res.expiration.toString());
          localStorage.setItem("userLogged", JSON.stringify(res.userLogged));
          this.feedbackService.success(res.message);
          this.configService.updateOptions();
          this.authorizeService.showMenuEvent.emit(true);
          
          this.router.navigate(['/'])
          
        }).catch(err=>{
          console.log(err);
          this.feedbackService.error(err.error);
        })
        .finally(()=>{
          this.loading = false;
        })
    }
  }

  isLogged(){
    this.authorizeService.validateUser()
      .then(res => {
        var token = localStorage.getItem("tokenSGE");
        var expiration = localStorage.getItem("expirationToken");
        var dateExpiration = new Date(expiration);
        var dateNow = new Date();
        
        if((token != null && token != '') && dateExpiration > dateNow){
          this.authorizeService.showMenuEvent.emit(true);
          this.router.navigate(["/"])
        }else{
          this.authorizeService.showMenuEvent.emit(false);
        }
      })
  }

  get nameForm() {
    return this.formLogin.get('name');
  }
  get passwordForm(){
    return this.formLogin.get('password');
  }
}
