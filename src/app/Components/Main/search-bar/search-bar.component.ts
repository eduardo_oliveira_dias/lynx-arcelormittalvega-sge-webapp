import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TagVariable } from 'src/app/Core/Models/TagVariable';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
import { VariableService } from 'src/app/Core/Services/variable.service';
import { faTag, faEdit } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  @Input("displaySearchBar") displaySearchBar;
  @Output('onHideBar') onHideBar = new EventEmitter<boolean>();
  variablesFound: TagVariable[];
  loadingVariables = false;
  faTag =faTag;
  faEdit = faEdit;
  searchedOnce = false;
  timeout;
  constructor(private variableService: VariableService,
              private feedbackService: FeedbackService, 
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
  }
  

  searchVariables(event){
    var value = event.srcElement.value.replace('+','');
    clearTimeout(this.timeout);
    if(value.length >= 3){
      this.timeout = setTimeout(()=>{
        this.loadingVariables = true;
        this.searchedOnce = true;
        this.variableService.getVariables(value)
          .then(res => {
            this.variablesFound = res;
          })
          .catch(err => {
            this.feedbackService.error(err.error);
          })
          .finally(() => {
            this.loadingVariables = false;
          })
      },1500)
    }
  }
  // goToEquipment(event, equipmentName){
  //   this.displaySearchBar = false;
  //   event.preventDefault();
  //   this.router.navigate(['equipment/'+equipmentName]);
  // }
  goToEquipment(equipmentName){
    this.displaySearchBar = false;
    this.router.navigate(['/equipment/'+equipmentName]);
  }

  hideBar(){
    this.onHideBar.emit(false);
  }
}
