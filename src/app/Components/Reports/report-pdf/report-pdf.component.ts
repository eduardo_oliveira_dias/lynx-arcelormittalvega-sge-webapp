import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { EChartsOption } from 'echarts';
import { ReportEquipments } from 'src/app/Core/Models/Report/ReportEquipments';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import html2pdf from 'html2pdf.js';

@Component({
  selector: 'app-report-pdf',
  templateUrl: './report-pdf.component.html',
  styleUrls: ['./report-pdf.component.css']
})
export class ReportPdfComponent implements OnInit {
  @Input('title') title: string = '';
  @Input('chartAlarmsByGroup') chartAlarmsByGroup: EChartsOption;
  @Input('chartOptionsKPIs') chartOptionsKPIs: EChartsOption;
  @Input('cols') cols: [] = [];
  @Input('reportEquipments') reportEquipments: ReportEquipments = new ReportEquipments();
  @Input('generatingReport') generatingReport: boolean;
  @Output('onGenerateReport') onGenerateReport = new EventEmitter<boolean>();
  @Output('onMountDocument') onMountDocument = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
    if(this.generatingReport){
      this.generateReport()
          
    }
  }

  generateReport(){
      let dateString = new Date().toLocaleString();
      var data = document.getElementById('report-body');

      var opt = {
        margin: 2,
        filename: 'report_equipments_'+dateString+'.pdf',
        image: {type: 'png', quality: 0.70},
        html2canvas: {scale: 2},
        jsPDF: {unit: 'mm', format:'a4', orientation:'portrait'}
      }

      html2pdf().from(data).set(opt)
        .then(() => {
          this.onMountDocument.emit(true);
        })
        .save()
        .thenExternal(() => {
        this.onGenerateReport.emit(true);
      });
     
  }

}
