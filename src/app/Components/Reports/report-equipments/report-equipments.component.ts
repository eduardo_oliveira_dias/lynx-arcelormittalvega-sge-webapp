import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faSearch, faEye, faRedoAlt, faFileAlt } from '@fortawesome/free-solid-svg-icons';
import { EChartsOption } from 'echarts';
import { Equipment } from 'src/app/Core/Models/Equipment';
import { GraphicBar } from 'src/app/Core/Models/Graphic/GraphicBar';
import { GraphicTrend } from 'src/app/Core/Models/Graphic/GraphicTrend';
import { PlantLevel } from 'src/app/Core/Models/PlantLevel';
import { ReportEquipments } from 'src/app/Core/Models/Report/ReportEquipments';
import { ReportFilter } from 'src/app/Core/Models/Request/ReportFilter';
import { DateService } from 'src/app/Core/Services/date.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
import { PlantLevelService } from 'src/app/Core/Services/plant-level.service';
import { ReportService } from 'src/app/Core/Services/report.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import html2pdf from 'html2pdf.js';
import { IntervalService } from 'src/app/Core/Services/interval.service';
@Component({
  selector: 'app-report-equipments',
  templateUrl: './report-equipments.component.html',
  styleUrls: ['./report-equipments.component.css']
})
export class ReportEquipmentsComponent implements OnInit {
  faSearch = faSearch;
  faEye = faEye;
  faRedoAlt = faRedoAlt;
  faFileAlt = faFileAlt;
  intervals = [];
  reportFilter : ReportFilter = new ReportFilter();
  formFilter : FormGroup;
  reportEquipments = new ReportEquipments();
  chartOptionsKPIs: EChartsOption;
  chartAlarmsByGroup: EChartsOption;
  chartDataTrendKPIs: GraphicTrend[];
  chartDataGroup: GraphicBar[];
  loadingReport: boolean;
  cols = [
    {field:'description', header:'Equipamento'},
    {field:'areaDescription', header:'Área'},
    {field:'timeStampSeverityStatus', header:'Data'},
    {field:'criticalStatusPeriod', header:'Período Crítico'},
    {field:'alertStatusPeriod', header:'Período Alerta'},
    {field:'severityStatus', header:'Status'},
    {field:'severityStatusDurationView', header:'Tempo no Status'}
  ];

  severities = [
    {label: 'CRITICO', value: 'CRITICO'},
    {label: 'ALERTA', value: 'ALERTA'},
    {label: 'NORMAL', value: 'NORMAL'},
  ]
  areaNames = [];
  equipmentNames = [];
  segments: PlantLevel[];
  areas: PlantLevel[];
  sections: PlantLevel[];

  loadingSegments = true;
  loadingAreas = true;
  loadingSections = true;
  paginator = true;
  sortColumns = true;
  filterRecords = true;
  showHeader = false;
  selectedStartTime: Date;
  selectedEndTime: Date;
  generateReport = false;
  classesGraphics = 'col-md-6 col-sm-6 card pt-2 pb-1 shadow-sm';
  innerGraphicsClasses = 'h-100';
  reportGenerated= false;
  constructor(private dateService: DateService, 
              private reportService: ReportService,
              private feedBackService: FeedbackService,
              private plantLevelService: PlantLevelService,
              private intervalService: IntervalService,
              private router: Router) { 
    this.reportFilter.endTime = new Date();
    this.reportFilter.startTime = this.dateService.add('days',this.reportFilter.endTime, -30);
    this.intervals = [{label:'Selecione', value:null}, ...this.intervalService.getIntervals()];

    this.formFilter = new FormGroup({
      startTime: new FormControl(this.reportFilter.startTime, [Validators.required]),
      endTime: new FormControl(this.reportFilter.endTime, [Validators.required]),
      segmentName: new FormControl(this.reportFilter.segmentName),
      areaName: new FormControl(this.reportFilter.areaName),
      sectionName: new FormControl(this.reportFilter.sectionName),
      interval: new FormControl('')

    });
  }

  ngOnInit(): void {
    // this.getReport();
    this.loadSegments();
  }
  

  getReport(){
    this.loadingReport = true;
    this.reportService.getReportEquipments(this.reportFilter)
      .then(res=>{
        if(res.equipmentsStatus.length > 0){
          this.selectedStartTime = this.reportFilter.startTime;
          this.selectedEndTime = this.reportFilter.endTime;
          this.reportEquipments = res;

          this.chartDataGroup = this.reportEquipments.graphicEquipmentStatus;
          this.chartDataTrendKPIs = this.reportEquipments.graphicKPIs;

          this.setChartDataAlarmsByGroup();
          this.setDataToGraphicKPI();
          this.setEquipments(this.reportEquipments.equipmentsStatus);
          this.reportGenerated = true;
        }else{
          this.feedBackService.warn('Não há registros no período selecionado.');
        }
        
      })
      .catch(err => {
        this.feedBackService.error(err.error);
      })
      .finally(() => {
        this.loadingReport = false;
      })
  }

  setEquipments(equipments: Equipment[]){
    if(equipments.length < 1){
      this.feedBackService.warn("Dados insuficientes para a tabela de equipamentos.");
      return;
    }
    this.areaNames = this.setOptions('areaDescription', equipments);
    this.equipmentNames = this.setOptions('description', equipments);
  }

  setOptions(optionName : string, list: Equipment[]){
    var uniqueOptionsList = new Set(list.map(equipment => equipment[optionName]));

    return Array.from(uniqueOptionsList).filter(x =>x!=null).map(optionName =>{ return {label: optionName, value: optionName }});
  }

  setChartDataAlarmsByGroup(){
    if(this.chartDataGroup.length < 1){
      this.feedBackService.warn("Dados insuficientes para o gráfico de barras.");
      return;
    }
    this.chartAlarmsByGroup = {
      tooltip: {
          trigger: 'item'
      },
      legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle:{fontSize:10}
      },
      
      color:["#E81818", "#fdd835", "#70AD47"],
      xAxis: [
        {
            type: 'category',
            axisTick: {show: false},
            axisLabel:{interval: 0},
            splitLine: {show:true},
            data: this.chartDataGroup.map(x => {
              var val = (x.description != null && x.description != '') ? x.description : x.info;

              return { value: val, textStyle: {fontSize: 12, width: 95, overflow: 'break'} }
            })
        }
      ],
      yAxis: [
          {
              type: 'value'
          }
      ],
      series: [ {
            name: 'CRITICO',
            type: 'bar',
            barGap:0,
            label: {
                show:true,
                fontSize: 10,
                fontWeight: 'bold',
                position: 'top'
            },
            data: this.chartDataGroup.map(chartData => {
              var item = chartData.items.filter(x => x['name'] == 'CRITICO')[0];
               return {
                 name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
                 value: (item != null && item != undefined)  ? item['value'] : 0
                };
            })
        },
        {
          name: 'ALERTA',
          type: 'bar',
          barGap:0,
          label: {
            show:true,
            fontSize: 10,
            fontWeight: 'bold',
            position: 'top'
          },
          data: this.chartDataGroup.map(chartData => {
            var item = chartData.items.filter(x => x['name'] == 'ALERTA')[0];
               return {
                 name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
                 value: (item != null && item != undefined)  ? item['value'] : 0,
                 itemData: chartData
                };
          })
       },
       {
        name: 'NORMAL',
        type: 'bar',
        barGap:0,
        label: {
          show:true,
          fontSize: 10,
          fontWeight: 'bold',
          position: 'top'
        },
        data: this.chartDataGroup.map(chartData => {
           var item = chartData.items.filter(x => x['name'] == 'NORMAL')[0];
           return {
             name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
             value: (item != null && item != undefined)  ? item['value'] : 0
            };
          })
       }
      ]
    };
    
  }

  setDataToGraphicKPI(){
    let series = [];
    let legendData = [];
    let xAxisData = [];
    if(this.chartDataTrendKPIs.length < 1){
      this.feedBackService.warn("Dados insuficientes para o gráfico de KPIs");
    }else{
      series = this.chartDataTrendKPIs.map(graphic => {
        return {
            name: (graphic.description != null && graphic.description != '') ? graphic.description : graphic.info,
            data: graphic.items.map(i => <number>i.value.toFixed(3)),
            type:'line',
            smooth:false,
            top:'10'
          }
        });
      legendData = this.chartDataTrendKPIs.map(i => (i.description != null && i.description != '') ? i.description : i.info);
      xAxisData = this.chartDataTrendKPIs[0].items.map(i => i.name);
    }
    this.chartOptionsKPIs = {
      tooltip: {
          trigger: 'axis'
      },
      legend: {
          data: legendData,
          show:true,
          textStyle:{fontSize:9}
      },
      xAxis: {
          type: 'category',
          boundaryGap: false,
          data: xAxisData
      },
      yAxis: {
          type: 'value'
      },
      series: series
    }
  }

  loadSegments(){
    this.loadingSegments = true;
    this.areas = [];
    this.sections = [];

    this.reportFilter.segmentName = null;
    this.reportFilter.areaName = null;
    this.reportFilter.sectionName = null;

    this.plantLevelService.getPlantLevelsByLevelName('Segment')
      .then(res => {
        this.segments = res;
      })
      .catch(err =>{
        console.log(err.error);
      })
      .finally(() =>{
        this.loadingSegments = false;
      })
  }

  loadAreas(){
    this.loadingAreas = true;
    this.loadingSections = true;
    this.areas = [];
    this.sections = [];

    this.reportFilter.areaName = null;
    this.reportFilter.sectionName = null;

    this.plantLevelService.getPlantLevelsByLevelName('Area', this.reportFilter.segmentName)
      .then(res => {
        this.areas = res;
      })
      .catch(err =>{
        console.log(err.error);
      })
      .finally(() =>{
        this.loadingAreas = false;
      })
  }

  loadSections(){
    this.loadingSections = true;
    this.sections = [];
    this.reportFilter.sectionName = null;

    this.plantLevelService.getPlantLevelsByLevelName('Section', this.reportFilter.areaName)
      .then(res => {
        this.sections = res;
      })
      .catch(err =>{
        console.log(err.error);
      })
      .finally(() =>{
        this.loadingSections = false;
      })
  }

  loadEquipment(event, equipment: Equipment){
    this.router.navigate(['equipment/'+equipment.name]);
  }

  captureScreen()  
  {  
    this.classesGraphics="col-md-12 col-sm-12 card pt-2 pb-1 shadow-sm";
    this.innerGraphicsClasses = 'h-100 d-flex justify-content-center';
    this.generateReport = true;
    this.paginator = false;
    this.showHeader = true;
  }

  setInterval(intervalValue){
    var Dates = this.intervalService.setInterval(intervalValue);
    this.reportFilter.startTime = Dates.inicio;
    this.reportFilter.endTime = Dates.fim;
  }

  hidePdf(){
    this.generateReport = false;
    this.paginator = true;
  }
  
  resetContent(){
    this.classesGraphics = 'col-md-6 col-sm-6 card pt-2 pb-1 shadow-sm';
    this.innerGraphicsClasses = 'h-100';
    this.showHeader = false;
  }

  get reportPeriod(){
    if(this.reportEquipments.equipmentsStatus != null){
      return ' - '+this.selectedStartTime.toLocaleDateString()+' - '+this.selectedEndTime.toLocaleDateString();
    }
    else{
      return '';
    }
  }
}
