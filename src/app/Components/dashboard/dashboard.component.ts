import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {faSearch, faEye, faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { EChartsOption } from 'echarts';
import { Alarm } from 'src/app/Core/Models/Alarm';
import { GraphicBar } from 'src/app/Core/Models/Graphic/GraphicBar';
import { GraphicSector } from 'src/app/Core/Models/Graphic/GraphicSector';
import { Level } from 'src/app/Core/Models/Level';
import { AlarmRequest } from 'src/app/Core/Models/Request/AlarmRequest';
import { RequestGraphicBar } from 'src/app/Core/Models/Request/RequestGraphicBar';
import { RequestGraphicSector } from 'src/app/Core/Models/Request/RequestGraphicSector';
import { AlarmService } from 'src/app/Core/Services/alarm.service';
import { DateService } from 'src/app/Core/Services/date.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
import { GraphicService } from 'src/app/Core/Services/graphic.service';
import { IntervalService } from 'src/app/Core/Services/interval.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  startTime = new Date();
  formSearch: FormGroup;
  faSearch = faSearch;
  faEye = faEye;
  faArrowLeft = faArrowLeft;
  chartAlarmsByType: EChartsOption;
  chartAlarmsByGroup: EChartsOption;
  chartDataType: GraphicSector;
  chartDataGroup: GraphicBar[];
  requestGraphicSector: RequestGraphicSector;
  loadingGraphicGroup: boolean;
  loadingGraphicType: boolean;
  alarms : Alarm[];
  graphicAlarms: Alarm[] = [];

  cols = [
    {field:'areaDescription', header:'Área'},
    {field:'equipmentName', header:'Cód. Equipamento'},
    {field:'equipmentDescription', header:'Equipamento'},
    {field:'variableName', header:'Tag'},
    {field:'variableDescription', header:'Descrição Tag'},
    {field:'value', header:'Valor'},
    {field:'severity', header:'Severidade'}
  ];

  severities = [
    {label: 'CRITICO', value: 'CRITICO'},
    {label: 'ALERTA', value: 'ALERTA'},
    {label: 'NORMAL', value: 'NORMAL'},
  ]

  variableNames = [];
  variableDescs = [];
  areaNames = [];
  equipmentNames = [];
  equipmentDescs = [];
  
  level = Level.Plant;

  historyGraphicBar = [];
  historyGraphicType = [];
  historyAlarms = [];
  
  listDescription = [];

  constructor(private graphicService: GraphicService,
        private feedBackService: FeedbackService,
        private intervalService: IntervalService,
        private dateService: DateService,
        private router: Router) { 
      var currentDate = this.intervalService.setInterval(2); //Mês atual
      this.startTime = currentDate.inicio;
      
  }

  ngOnInit(): void {
    this.formSearch = new FormGroup({
      startTime: new FormControl(this.startTime, [Validators.required])
    });

    this.searchGraphicSectorAlarms();
    this.searchGraphicBarByGroup();
  }

  setChartDataAlarmByType(){
    this.chartAlarmsByType = {
      tooltip: {
          trigger: 'item'
      },
      legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle:{fontSize:10}
      },
      
      color:["#ED7D31", "#E81818", "#70AD47"],
      series: [
          {
              name: this.chartDataType.info,
              type: 'pie',
              radius: ['30%','85%'],
              left: "5%",
              label: {
                  formatter: '{d}% ({c})',
                  fontSize: 14,
                  fontWeight: 'bold',
                  position:'inside'
              },
              labelLine:{length: 1},
              emphasis: {
                  label: {
                      show: true,
                      fontSize: '10',
                      fontWeight: 'bold'
                  }
              },
              data: this.chartDataType.items.map(x => {
                var color = '#4472C4';
                if(x['name'] == 'NORMAL') color = '#70AD47';
                else if(x['name'] == 'CRITICO') color = '#E81818';
                else if(x['name'] == 'ALERTA') color = '#fdd835';
                return {
                        name: x['name'], 
                        value: x['value'],
                        itemStyle:{color: color}
                      }
              })
          }
      ]
    };
  }

  setChartDataAlarmsByGroup(){
    this.chartAlarmsByGroup = {
      tooltip: {
          trigger: 'item'
      },
      legend: {
          itemHeight: 10,
          itemWidth: 10,
          textStyle:{fontSize:10}
      },
      
      color:["#E81818", "#fdd835", "#70AD47"],
      xAxis: [
        {
            type: 'category',
            axisTick: {show: false},
            axisLabel:{interval: 0},
            splitLine: {show:true},
            data: this.chartDataGroup.map(x => {
              var val = (x.description != null && x.description != '') ? x.description : x.info;

              return { value: val, textStyle: {fontSize: 12, width: 95, overflow: 'break'} }
            })
        }
      ],
      yAxis: [
          {
              type: 'value'
          }
      ],
      series: [ {
            name: 'CRITICO',
            type: 'bar',
            barGap:0,
            label: {
                show:true,
                fontSize: 10,
                fontWeight: 'bold',
                position: 'top'
            },
            data: this.chartDataGroup.map(chartData => {
              var item = chartData.items.filter(x => x['name'] == 'CRITICO')[0];
               return {
                 name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
                 value: (item != null && item != undefined)  ? item['value'] : 0
                };
            })
        },
        {
          name: 'ALERTA',
          type: 'bar',
          barGap:0,
          label: {
            show:true,
            fontSize: 10,
            fontWeight: 'bold',
            position: 'top'
          },
          data: this.chartDataGroup.map(chartData => {
            var item = chartData.items.filter(x => x['name'] == 'ALERTA')[0];
               return {
                 name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
                 value: (item != null && item != undefined)  ? item['value'] : 0,
                 itemData: chartData
                };
          })
       },
       {
        name: 'NORMAL',
        type: 'bar',
        barGap:0,
        label: {
          show:true,
          fontSize: 10,
          fontWeight: 'bold',
          position: 'top'
        },
        data: this.chartDataGroup.map(chartData => {
           var item = chartData.items.filter(x => x['name'] == 'NORMAL')[0];
           return {
             name: (chartData.description != null && chartData.description != '') ? chartData.description: chartData.info,
             value: (item != null && item != undefined)  ? item['value'] : 0
            };
          })
       }
      ]
    };
    
  }


  setOptions(optionName : string, list: Alarm[]){
    var uniqueOptionsList = new Set(list.map(alarm => alarm[optionName]));

    return Array.from(uniqueOptionsList).filter(x =>x!=null).map(optionName =>{ return {label: optionName, value: optionName }});
  }

  searchAlarms(alarms: Alarm[]){
      this.alarms = alarms;
      this.variableDescs = this.setOptions('variableDescription', alarms);
        this.variableNames = this.setOptions('variableName', alarms);
        this.areaNames = this.setOptions('areaDescription', alarms);
        this.equipmentNames = this.setOptions('equipmentName', alarms);
        this.equipmentDescs = this.setOptions('equipmentDescription', alarms);
  }

  searchGraphicSectorAlarms(newSearch = true, itemSelected = null){
    if(newSearch){
      this.loadingGraphicType = true;
      this.requestGraphicSector = new RequestGraphicSector();
      this.requestGraphicSector.startTime = this.startTime;
      this.requestGraphicSector.endTime = this.endTime;
      this.graphicService.getAlarmsByType(this.requestGraphicSector)
        .then(res => {
          this.chartAlarmsByType = {};
          this.chartDataType = res.graphicData;
          this.historyGraphicType.push(this.chartDataType);
          this.setChartDataAlarmByType();
        })
        .catch(err =>{
          this.feedBackService.error(err.error);
        })
        .finally(()=>{
          this.loadingGraphicType = false;
        })
    }else{
      // this.loadingGraphicType = true;
      this.requestGraphicSector = new RequestGraphicSector();
      this.requestGraphicSector.startTime = this.startTime;
      this.requestGraphicSector.endTime = this.endTime;
      this.requestGraphicSector.level = this.level;
      this.requestGraphicSector.levelName = itemSelected.info;
      this.requestGraphicSector.alarms = this.graphicAlarms;
      this.graphicService.mountAlarmsByType(this.requestGraphicSector)
        .then(res => {
          this.chartAlarmsByType = {};
          this.chartDataType = res.graphicData;
          this.historyGraphicType.push(this.chartDataType);
          this.setChartDataAlarmByType();
        })
        .catch(err =>{
          this.feedBackService.error(err.error);
        })
        .finally(()=>{
          this.loadingGraphicType = false;
        })
    }
  }

  searchGraphicBarByGroup(){
    this.loadingGraphicGroup = true;
    var requestGraphicBar = new RequestGraphicBar();
    requestGraphicBar.startTime = this.startTime;
    requestGraphicBar.endTime = this.endTime;
    this.graphicService.getAlarmsByGroup(requestGraphicBar)
      .then(res => {
        // this.listRequestGraphicBar.push(requestGraphicBar);
        this.chartAlarmsByGroup = {};
        this.chartDataGroup = res.graphicData;
        this.historyGraphicBar.push(this.chartDataGroup);
        this.graphicAlarms = res.alarms;
        this.historyAlarms.push(res.alarms);
        this.setChartDataAlarmsByGroup();
        this.searchAlarms(res.alarms);
      })
      .catch(err =>{
        this.feedBackService.error(err.error);
      })
      .finally(()=>{
        this.loadingGraphicGroup = false;
      })
  }

  loadEquipment(event, alarm: Alarm){
    this.router.navigate(['equipment/'+alarm.equipmentName]);
  }

  get startTimeForm(){
    return this.formSearch.get('startTime');
  }

  get endTime() : Date{
    return this.dateService.add('months', this.startTime, 1);
  }

  searchAll(){
    this.level = Level.Plant;
    this.historyGraphicBar = [];
    this.historyAlarms = [];
    this.alarms = [];
    this.searchGraphicSectorAlarms();
    this.searchGraphicBarByGroup();
  }

  drilldownChartBar(itemBar){
    var itemSelected = this.chartDataGroup.find(x => (x.description == itemBar.data.name) || (x.info == itemBar.data.name))

    if(itemSelected == null){
      console.log("Não Encontrado.");
      return;
    }
    if(this.level != Level.Section){
      this.getNextLevel();

      if(this.graphicAlarms.length > 0){
        //this.loadingGraphicGroup = true;
        var requestGraphicBar = new RequestGraphicBar();
        requestGraphicBar.startTime = this.startTime;
        requestGraphicBar.endTime = this.endTime;
        requestGraphicBar.level = this.level;
        requestGraphicBar.levelName = itemSelected.info;
        requestGraphicBar.alarms = this.graphicAlarms;
        this.searchGraphicSectorAlarms(false,itemSelected);
        this.graphicService.mountAlarmsByGroup(requestGraphicBar)
          .then(res => {
            // this.listRequestGraphicBar.push(requestGraphicBar);
            this.listDescription.push((itemSelected.description != null || itemSelected.description != '') ? itemSelected.description : itemSelected.info);
            this.chartAlarmsByGroup = {};
            this.chartDataGroup = res.graphicData;
            this.historyGraphicBar.push(this.chartDataGroup);
            this.graphicAlarms = res.alarms;
            this.historyAlarms.push(res.alarms);
            this.setChartDataAlarmsByGroup();
            this.searchAlarms(res.alarms);

          })
          .catch(err =>{
            this.feedBackService.error(err.error);
          })
          .finally(()=>{
            this.loadingGraphicGroup = false;
          })
      }else{
        //this.loadingGraphicGroup = true;
        var requestGraphicBar = new RequestGraphicBar();
        requestGraphicBar.startTime = this.startTime;
        requestGraphicBar.endTime = this.endTime;
        requestGraphicBar.level = this.level;
        requestGraphicBar.levelName = itemSelected.info;
    
        this.searchGraphicSectorAlarms();
        this.graphicService.getAlarmsByGroup(requestGraphicBar)
          .then(res => {
            // this.listRequestGraphicBar.push(requestGraphicBar);
            this.listDescription.push((itemSelected.description != null || itemSelected.description != '') ? itemSelected.description : itemSelected.info);
            this.chartAlarmsByGroup = {};
            this.chartDataGroup = res.graphicData;
            this.historyGraphicBar.push(this.chartDataGroup);
            this.graphicAlarms = res.alarms;
            this.historyAlarms.push(res.alarms);
            this.setChartDataAlarmsByGroup();
            this.searchAlarms(res.alarms);

          })
          .catch(err =>{
            this.feedBackService.error(err.error);
          })
          .finally(()=>{
            this.loadingGraphicGroup = false;
          })
      }
    }
    
    
  }

  drillUpChartbar(){
    this.historyGraphicBar.pop();
    this.historyGraphicType.pop();
    this.historyAlarms.pop();
    if(this.historyGraphicBar.length >= 1){
      this.chartDataGroup = this.historyGraphicBar[this.historyGraphicBar.length - 1];
      this.chartDataType = this.historyGraphicType[this.historyGraphicType.length - 1];
      this.graphicAlarms = this.historyAlarms[this.historyAlarms.length - 1];
      this.getPrevLevel();
      this.setChartDataAlarmsByGroup();
      this.setChartDataAlarmByType();
      this.searchAlarms(this.graphicAlarms);

    }
  }

  getNextLevel(){
    switch(this.level){
      case Level.Plant:
        this.level = Level.Segment;
        break;
      case Level.Segment:
        this.level = Level.Area;
        break;
      case Level.Area:
        this.level = Level.Section;
        break;
    }
  }

  getPrevLevel(){
    switch(this.level){
      case Level.Segment:
        this.level = Level.Plant;
        break;
      case Level.Area:
        this.level = Level.Segment;
        break;
      case Level.Section:
        this.level = Level.Area;
        break;
    }
  }

  getColumnWidth(columnField){
    
    switch(columnField){
      case 'value': return '8%';
      case 'severity': return '12%';
      default: return 'initial';

    }
  }

  get lastLevel(){
    return this.listDescription[this.listDescription.length - 1];
  }

  get currentLevelChildrenName(){
    var name = '';
    switch(this.level){
      case Level.Plant:
        name = 'Segmento';
        break;
      case Level.Segment:
        name = 'Área';
        break;
      case Level.Area:
        name = 'Seção';
        break;
      case Level.Section:
        name = 'Equipamento';
    }

    return name;
  }

  get backLevelChildrenName(){
    var name = '';
    switch(this.level){
      case Level.Plant:
        name = 'Planta';
        break;
      case Level.Segment:
        name = 'Segmento';
        break;
      case Level.Area:
        name = 'Área';
        break;
      case Level.Section:
        name = 'Seção';
    }

    return name;
  }

}
