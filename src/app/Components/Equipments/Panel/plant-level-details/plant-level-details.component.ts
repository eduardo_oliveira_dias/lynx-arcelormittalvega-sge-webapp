import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange } from '@angular/core';
import { AlarmService } from 'src/app/Core/Services/alarm.service';
import { GraphicService } from 'src/app/Core/Services/graphic.service';
import { faTimes, faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { EChartsOption } from 'echarts';
import { Alarm } from 'src/app/Core/Models/Alarm';
import { CardLevel } from 'src/app/Core/Models/CardLevel';
import { IndicatorService } from 'src/app/Core/Services/indicator.service';
import { Indicator } from 'src/app/Core/Models/Indicator';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plant-level-details',
  templateUrl: './plant-level-details.component.html',
  styleUrls: ['./plant-level-details.component.css']
})
export class PlantLevelDetailsComponent implements OnInit, OnChanges {
  @Input('visible') visible: boolean;
  @Input('cardLevel') cardLevel: CardLevel
  @Output('onHideModal') onHideModal = new EventEmitter<boolean>();
  faTimes = faTimes;
  faEdit = faEdit;
  faEye = faEye;
  chartOption: EChartsOption;
  alarms : Alarm[];
  dataReference: Date;
  cols = [
    {field:'areaDescription', header:'Área'},
    {field:'equipmentName', header:'Cód. Equipamento'},
    {field:'equipmentDescription', header:'Equipamento'},
    {field:'variableName', header:'Tag'},
    {field:'variableDescription', header:'Descrição Tag'},
    {field:'value', header:'Valor'},
    {field:'severity', header:'Severidade'}
  ];

  severities = [
    {label: 'CRITICO', value: 'CRITICO'},
    {label: 'ALERTA', value: 'ALERTA'},
    {label: 'NORMAL', value: 'NORMAL'},
  ]

  variableNames = [];
  variableDescs = [];
  areaNames = [];
  equipmentNames = [];
  equipmentDescs = [];

  loadingAlarms = false;
  kpi: Indicator = new Indicator();
  constructor(private graphicService: GraphicService, 
            private alarmService: AlarmService, 
            private indicatorService: IndicatorService,
            private router: Router) { 

  }

  ngOnInit(): void {
    this.dataReference = new Date();
  }

  ngOnChanges(changes){
      if(changes.cardLevel ){
        if(changes.cardLevel.previousValue == undefined || (changes.cardLevel.currentValue.name != changes.cardLevel.previousValue.name)){
          this.alarms = [];
          this.cardLevel = changes.cardLevel.currentValue;
          this.chartOption = {
            tooltip: {
                trigger: 'item'
            },
            legend: {
                top:'bottom'
            },
            
            color:["#ED7D31", "#E81818", "#70AD47"],
            series: [
                {
                    name: "Alarmes",
                    type: 'pie',
                    radius: ['30%','70%'],
                    //bottom: "20%",
                    label: {
                      formatter: '{d}% ({c})',
                      position: 'inside',
                      fontWeight: 'bold'
                    },
                    labelLine:{length: 1},
                    emphasis: {
                        label: {
                            show: true,
                            fontSize: '10',
                            fontWeight: 'bold'
                        }
                    },
                    data: this.cardLevel.chartData.map(x => {
                      var color = '#4472C4';
                      if(x['name'] == 'NORMAL') color = '#70AD47';
                      else if(x['name'] == 'CRITICO') color = '#E81818';
                      else if(x['name'] == 'ALERTA') color = '#fdd835';
                      return {
                              name: x['name'], 
                              value: x['value'],
                              itemStyle:{color: color}
                            }
                    })
                }
            ]
          };
          
          this.loadingAlarms = true;
          this.indicatorService.getKPI(this.cardLevel.name)
            .then(res =>{
              this.kpi = res;
            })
            .catch(err=>{
              console.log(err.error);
            })
          this.alarmService.getAlarmsByName(this.cardLevel.level, this.cardLevel.name)
            .then(alarms => {
              this.loadingAlarms = false;
              this.alarms = alarms;
              this.variableNames = this.setOptions('variableDescription', alarms);
              this.areaNames = this.setOptions('areaDescription', alarms);
              this.equipmentNames = this.setOptions('equipmentName', alarms);
              this.equipmentDescs = this.setOptions('equipmentDescription', alarms);
              
            })
            .catch(error =>{
              this.loadingAlarms = false;
            });
        }
      }

  }

  setOptions(optionName : string, list: Alarm[]){
    var uniqueOptionsList = new Set(list.map(alarm => alarm[optionName]));

    return Array.from(uniqueOptionsList).filter(x =>x!=null).map(optionName =>{return {label: optionName, value: optionName}});
  }

  hideModal(event){
      this.onHideModal.emit(true);
  }

  loadEquipment(event, alarm: Alarm){
    this.router.navigate(['equipment/'+alarm.equipmentName]);
  }

  getColumnWidth(columnField){
    
    switch(columnField){
      case 'value': return '8%';
      case 'severity': return '12%';
      default: return 'initial';

    }
  }
}
