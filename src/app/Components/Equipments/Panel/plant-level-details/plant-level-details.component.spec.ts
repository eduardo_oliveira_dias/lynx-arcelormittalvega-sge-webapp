import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantLevelDetailsComponent } from './plant-level-details.component';

describe('PlantLevelDetailsComponent', () => {
  let component: PlantLevelDetailsComponent;
  let fixture: ComponentFixture<PlantLevelDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlantLevelDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantLevelDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
