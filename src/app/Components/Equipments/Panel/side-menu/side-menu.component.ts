import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { faCogs, faBars, faTimes, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { PlantLevelService } from 'src/app/Core/Services/plant-level.service';
import { PlantLevel } from 'src/app/Core/Models/PlantLevel';
import { hierarchyLevels } from 'src/app/Core/Models/util/hierarchyLevels';
import { Router } from '@angular/router';


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  faCogs = faCogs;
  faBars = faBars;
  faArrowLeft = faArrowLeft;
  collapsed = true;
  data: TreeNode[];
  rootPlantLevel: PlantLevel;
  selectedNode: TreeNode;
  @Output('onCollapse') onCollapse =  new EventEmitter<boolean>();
  @Output('onChangeLevel') onChangeLevel = new EventEmitter<object>();

  constructor(private plantLevelService: PlantLevelService, private router: Router) { 
    this.plantLevelService.getPlantLevels()
      .then(plantLevel => {
        this.rootPlantLevel = plantLevel;
        this.data = this.PlantLevelToTreeNode(this.rootPlantLevel);

      }).catch(error =>{
        console.log(error);
      })
  }

  ngOnInit(): void {

  }

  selectNode(event){
    var historyLevels = this.mountPath(event.node);
  
    if(historyLevels.length <= 4){
      var historyLevelNames = this.mountDescriptionPath(event.node);
      var name = event.node.data.name;
      var description = event.node.data.description;
      var childrenLevel = hierarchyLevels[historyLevels.length - 1];
      var objectChangeLevel = {
        name,
        description,
        historyLevels,
        historyLevelNames,
        childrenLevel
      }
      
      this.onChangeLevel.emit(objectChangeLevel);
    }else if(event.node.children.length < 1){
      this.loadEquipment(event.node.data.name);
    }
  }

  loadEquipment(name){
    this.router.navigate(['equipment/'+name]);
  }

  mountPath(element){
    var path = [];
    path.splice(0,0,element.data.name);
    while(element.parent != null){
      element = element.parent;
      path.splice(0,0,element.data.name);
    }

    return path;
  }
  mountDescriptionPath(element){
    var path = [];
    path.splice(0,0,element.data.description);
    while(element.parent != null){
      element = element.parent;
      path.splice(0,0,element.data.description);
    }

    return path;
  }

  PlantLevelToTreeNode(plantLevel: PlantLevel, obj = false){
    var data;
    if(!obj){
      data = [
        {
          data: plantLevel,
          children: plantLevel.children.map(x => this.PlantLevelToTreeNode(x, true))
        }
      ]
    }else{
      data = {
          data: plantLevel,
          children: plantLevel.children.map(x => this.PlantLevelToTreeNode(x, true))
        }
    }
    return data;
  }
  collapseBar(){
    this.collapsed = !this.collapsed;
    this.onCollapse.emit(this.collapsed);
  }
}
