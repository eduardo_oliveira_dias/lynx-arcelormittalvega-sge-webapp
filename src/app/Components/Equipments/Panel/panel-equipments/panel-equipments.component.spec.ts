import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelEquipmentsComponent } from './panel-equipments.component';

describe('PanelEquipmentsComponent', () => {
  let component: PanelEquipmentsComponent;
  let fixture: ComponentFixture<PanelEquipmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PanelEquipmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelEquipmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
