import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panel-equipments',
  templateUrl: './panel-equipments.component.html',
  styleUrls: ['./panel-equipments.component.css']
})
export class PanelEquipmentsComponent implements OnInit {
  classSideNav = 'side-nav collapsed';
  collapsedSideNav:boolean;
  objectChangelevel: object;
  constructor() { }

  ngOnInit(): void {
  }
  
  collapseSideNav(value){
    if(value) this.classSideNav = 'side-nav collapsed';
    else this.classSideNav = 'side-nav expanded';
    this.collapsedSideNav = value;
  }

  changeLevel(objectChangeLevel){
    this.objectChangelevel = objectChangeLevel;
  }
}
