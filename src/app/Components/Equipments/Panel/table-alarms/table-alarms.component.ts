import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { faCalendarAlt, faSearch, faEdit, faEye } from '@fortawesome/free-solid-svg-icons';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Alarm } from 'src/app/Core/Models/Alarm';
import { AlarmRequest } from 'src/app/Core/Models/Request/AlarmRequest';
import { AlarmService } from 'src/app/Core/Services/alarm.service';
import { DateService } from 'src/app/Core/Services/date.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';

@Component({
  selector: 'app-table-alarms',
  templateUrl: './table-alarms.component.html',
  styleUrls: ['./table-alarms.component.css']
})
export class TableAlarmsComponent implements OnInit, OnChanges {
  faCalendarAlt = faCalendarAlt;
  faSearch = faSearch;
  faEdit = faEdit;
  faEye = faEye;
  alarms : Alarm[];
  @Input('objectChangeLevel') objectChangeLevel: object;
  levelName = 'BV';
  level = 'plant';
  cols = [
    {field:'areaDescription', header:'Área'},
    {field:'equipmentName', header:'Cód. Equipamento'},
    {field:'equipmentDescription', header:'Equipamento'},
    {field:'variableName', header:'Tag'},
    {field:'variableDescription', header:'Descrição Tag'},
    {field:'value', header:'Valor'},
    {field:'severity', header:'Severidade'}
  ];

  severities = [
    {label: 'CRITICO', value: 'CRITICO'},
    {label: 'ALERTA', value: 'ALERTA'},
    {label: 'NORMAL', value: 'NORMAL'},
  ]

  variableNames = [];
  variableDescs = [];
  areaNames = [];
  equipmentNames = [];
  equipmentDescs = [];

  startTime: Date; endTime:Date;
  loading = false;
  formSearch: FormGroup;
  alarmsLabel = 'Alarmes Ativos';
  constructor(private bsLocaleService: BsLocaleService, 
              private alarmService: AlarmService,
              private router: Router,
              private dateService: DateService,
              private feedbackService: FeedbackService) {
    
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(
        changes.objectChangeLevel!= undefined &&
        (!changes.objectChangeLevel.firstChange && changes.objectChangeLevel.currentValue.name != this.levelName)
      ){
        this.objectChangeLevel = changes.objectChangeLevel.currentValue;
        this.level = this.objectChangeLevel['childrenLevel']['name'];
        this.levelName = this.objectChangeLevel['name'];
        this.alarms = [];
        this.loading = true;
        this.alarmService.getAlarmsByName(this.level,this.levelName)
          .then(alarms => {
            this.loading = false;
            this.alarms = alarms;
            
            this.variableDescs = this.setOptions('variableDescription', alarms);
            this.variableNames = this.setOptions('variableName', alarms);
            this.areaNames = this.setOptions('areaDescription', alarms);
            this.equipmentNames = this.setOptions('equipmentName', alarms);
            this.equipmentDescs = this.setOptions('equipmentDescription', alarms);

            this.alarmsLabel = "Alarmes Ativos";
          })
          .catch(error =>{
            this.loading = false;
            console.log(error);
          })
      }
  }

  ngOnInit(): void {
    this.endTime = new Date();
    this.startTime = this.dateService.add('days',this.endTime, -3);

    this.formSearch = new FormGroup({
        startTime: new FormControl(this.startTime,[Validators.required]),
        endTime: new FormControl(this.endTime, [Validators.required]),
       })
    this.bsLocaleService.use('pt-br');
    this.loading = true;
    this.alarmService.getAlarmsByName(this.level,this.levelName)
      .then(alarms => {
        this.loading = false;
        this.alarms = alarms;
        
        this.variableDescs = this.setOptions('variableDescription', alarms);
        this.variableNames = this.setOptions('variableName', alarms);
        this.areaNames = this.setOptions('areaDescription', alarms);
        this.equipmentNames = this.setOptions('equipmentName', alarms);
        this.equipmentDescs = this.setOptions('equipmentDescription', alarms);
      })
      .catch(error =>{
        this.loading = false;
        console.log(error);
      })
  }

  setOptions(optionName : string, list: Alarm[]){
    var uniqueOptionsList = new Set(list.map(alarm => alarm[optionName]));

    return Array.from(uniqueOptionsList).filter(x =>x!=null).map(optionName =>{ return {label: optionName, value: optionName }});
  }

  loadEquipment(event, alarm: Alarm){
    this.router.navigate(['equipment/'+alarm.equipmentName]);
  }
  
  searchAlarms(){
    this.loading = true;
    this.alarms = [];
    var request = new AlarmRequest();
    request.endTime = this.endTime;
    request.startTime = this.startTime;

    if(this.objectChangeLevel != undefined){
      request.level = this.objectChangeLevel['childrenLevel']['name'];
      request.levelName = this.objectChangeLevel['name'];
    }

    this.alarmService.getTrendAlarms(request)
      .then(alarms => {
        this.alarms = alarms;
        this.variableDescs = this.setOptions('variableDescription', alarms);
        this.variableNames = this.setOptions('variableName', alarms);
        this.areaNames = this.setOptions('areaDescription', alarms);
        this.equipmentNames = this.setOptions('equipmentName', alarms);
        this.equipmentDescs = this.setOptions('equipmentDescription', alarms);

        this.alarmsLabel = "Alarmes no período: "+this.startTime.toLocaleString()+" - "+this.endTime.toLocaleString();
      })
      .catch(err => {
        this.feedbackService.error(err.error);
      })
      .finally(() => this.loading = false);
  }

  getColumnWidth(columnField){
    
    switch(columnField){
      case 'value': return '8%';
      case 'severity': return '12%';
      default: return 'initial';

    }
  }


  /**Getters */

  get startTimeForm(){
    return this.formSearch.get('startTime');
  }

  get endTimeForm(){
    return this.formSearch.get('endTime');
  }
}
