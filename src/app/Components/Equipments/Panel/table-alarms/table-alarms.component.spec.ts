import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableAlarmsComponent } from './table-alarms.component';

describe('TableAlarmsComponent', () => {
  let component: TableAlarmsComponent;
  let fixture: ComponentFixture<TableAlarmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableAlarmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableAlarmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
