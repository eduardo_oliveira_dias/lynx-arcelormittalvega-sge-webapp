import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { faPlus, faArrowLeft, faImages, faSave, faTimes, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { GraphicSector } from 'src/app/Core/Models/Graphic/GraphicSector';
import { GraphicService } from 'src/app/Core/Services/graphic.service';
import { hierarchyLevels} from 'src/app/Core/Models/util/hierarchyLevels';
import { FileService } from 'src/app/Core/Services/file.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CardLevel } from 'src/app/Core/Models/CardLevel';
import {MessageService} from 'primeng/api';
import { ConfigService } from 'src/app/Core/Services/config.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';


@Component({
  selector: 'app-image-panel',
  templateUrl: './image-panel.component.html',
  styleUrls: ['./image-panel.component.css']
})
export class ImagePanelComponent implements OnInit, OnChanges {
  faPlus = faPlus;
  faArrowUp = faArrowUp;
  faImages = faImages;
  faSave = faSave;
  faTimes = faTimes;
  graphicSectors: GraphicSector[];
  currentLevel: string = "BV";
  currentLevelName: string = "Brasil Vega";
  level = { name: "root", children:"segmentName"}
  childrenLevel = { name: "segmentName", children: "areaName" };
  historyLevels = [];
  historyLevelNames = [];
  imageFile: File;
  displayModal = false;
  fileBkg = null;
  styleBackGround = {};
  loadingCards=false;
  loadingImage=false;
  savingImage=false;
  selectedItem: CardLevel;
  visibleDetails:boolean;
  @Input('collapseSideBar') collapseSideBar: boolean;
  @Input('objectChangeLevel') objectChangeLevel : object;
  @Output('onChangeLevel') onChangeLevel: EventEmitter<object> = new EventEmitter<object>();

  constructor(private graphicService: GraphicService, 
      private fileService: FileService, 
      private sanitizer: DomSanitizer,
      private messageService: MessageService,
      private feedbackService: FeedbackService) {
  }

  ngOnInit(): void {
    this.loadingCards = true;
    
    this.graphicService.getGraphicSector(this.level.children, this.level.name)
      .then(graphicSectors =>{
        this.historyLevels.push(this.currentLevel);
        this.historyLevelNames.push(this.currentLevelName);
        this.childrenLevel = hierarchyLevels.find(x => x.name == this.level.children);
        this.graphicSectors = graphicSectors;
      })
      .catch(err => {
        this.feedbackService.error(err.error);
      })
      .finally(() =>{
        this.loadingCards = false;
      })

      this.loadBackground();
      
  }

  ngOnChanges(changes: SimpleChanges){
    if(
        changes.objectChangeLevel!= undefined &&
        (!changes.objectChangeLevel.firstChange && changes.objectChangeLevel.currentValue.name != this.currentLevel)
      ){
      this.objectChangeLevel = changes.objectChangeLevel.currentValue;

      this.historyLevels = this.objectChangeLevel['historyLevels'];
      this.historyLevelNames = this.objectChangeLevel['historyLevelNames'];
      this.currentLevel = this.objectChangeLevel['name'];
      this.currentLevelName = this.objectChangeLevel['description'];
      this.childrenLevel = this.objectChangeLevel['childrenLevel'];
      this.level = this.childrenLevel;

      this.loadingCards = true;
      this.graphicService.getGraphicSector(this.childrenLevel['children'], this.childrenLevel['name'], this.currentLevel)
        .then(graphicSectors =>{
          this.childrenLevel = hierarchyLevels.find(x => x.name == this.level.children);
          this.graphicSectors = graphicSectors;
          this.loadBackground();
        })
        .catch(err=>{
          this.feedbackService.error(err.error);  
        }).finally(()=>{
          this.loadingCards = false;
        })

    }
  }

  loadChild(child: CardLevel){
    this.loadingCards = true;
    this.graphicService.getGraphicSector(this.childrenLevel['children'], this.childrenLevel['name'], child.name)
    .then(graphicSectors =>{
        this.currentLevel = child.name;
        this.historyLevels.push(child.name);
        this.currentLevelName = child.description;
        this.historyLevelNames.push(child.description);
        this.level = this.childrenLevel;
        this.childrenLevel = hierarchyLevels.find(x => x.name == this.level.children);
        
        this.setObjectChangeLevel();
        this.graphicSectors = graphicSectors;
        this.loadBackground();
      })
      .catch(err=>{
        console.log(err);
        this.feedbackService.error(err.error);  
      }).finally(()=>{
        this.loadingCards = false;
      })
  }

  backLevel(){
    this.loadingCards = true;
    var level = hierarchyLevels.find(x => x.children == this.level.name);
    this.historyLevels.pop();
    this.historyLevelNames.pop();
    this.currentLevel = this.historyLevels[this.historyLevels.length-1];
    this.currentLevelName = this.historyLevelNames[this.historyLevelNames.length-1];

    this.graphicService.getGraphicSector(level['children'], level['name'], this.historyLevels[this.historyLevels.length-1])
      .then(graphicSectors =>{
        this.level = level;
        this.childrenLevel = hierarchyLevels.find(x => x.name == this.level.children);

        this.setObjectChangeLevel();

        this.graphicSectors = graphicSectors;
        this.loadBackground();

        
      }).catch(err=>{
        this.feedbackService.error(err.error);  
      }).finally(()=>{
        this.loadingCards = false;
      });
  }


  showFormImage(){
    this.imageFile = null;
    this.displayModal = true;
    
  }

  onChange(event){
    var file = event.srcElement.files[0];
    if(file != null){
      this.imageFile = file; 
  
      document.getElementById('image-file-label').innerHTML = file.name;
    }
  }

  saveFile(){
    if(this.imageFile != null){
      this.savingImage = true;
      this.fileService.uploadImage(this.imageFile,this.currentLevel)
        .then(res => {
          this.displayModal = false;
          
          this.loadBackground();
        }).catch(err=>{
          this.feedbackService.error(err.error); 
        }).finally(()=>{
          this.savingImage = false;
        });
    }
  }

  loadBackground(){
    this.loadingImage = true;
    this.fileService.getImage(this.currentLevel)
      .then(res =>{
        
        var image ="";
        if(res.type == 'jpg') image = 'data:image/jpeg;base64,'+res.content;
        if(res.type == 'png') image = 'data:image/png;base64,'+res.content;

        this.fileBkg = res['image'];
        this.styleBackGround = {'background-image': 'url('+image+')', 'background-repeat':'no-repeat', 'background-size':'cover','background-position':'center'};

      })
      .catch(err=>{
        this.feedbackService.warn(err.error);
        this.styleBackGround = {'background-image': ''};
      })
      .finally(() =>{
        this.loadingImage = false;
      })
  }

  showDetails(cardLevel:CardLevel){
    cardLevel.level = this.level.children;
    this.selectedItem = cardLevel;
    this.visibleDetails = true;
  }

  showError(message: string){
    this.messageService.add({severity:'error', summary:'Erro', detail:message});
  }

  setObjectChangeLevel(){
    this.objectChangeLevel = {
      historyLevels: this.historyLevels,
      historyLevelNames: this.historyLevelNames,
      name: this.currentLevel,
      description: this.currentLevelName,
      childrenLevel: this.level
    }

    this.onChangeLevel.emit(this.objectChangeLevel);
  }

  resetInput(){
    
    try{
      var inputFile = document.getElementById('image-file-label');
      if(inputFile) inputFile.innerHTML = 'Escolher arquivo';

    }catch(e){
      console.log(e);
    }
  }
}
