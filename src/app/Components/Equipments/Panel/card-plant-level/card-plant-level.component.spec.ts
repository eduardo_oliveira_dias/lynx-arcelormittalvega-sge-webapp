import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPlantLevelComponent } from './card-plant-level.component';

describe('CardPlantLevelComponent', () => {
  let component: CardPlantLevelComponent;
  let fixture: ComponentFixture<CardPlantLevelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardPlantLevelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPlantLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
