import { CdkDrag } from '@angular/cdk/drag-drop';
import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Component, ElementRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus, faSearchPlus, faEye } from '@fortawesome/free-solid-svg-icons';
import { EChartsOption } from 'echarts';
import { CardLevel } from 'src/app/Core/Models/CardLevel';
import { ConfigService } from 'src/app/Core/Services/config.service';

@Component({
  selector: 'app-card-plant-level',
  templateUrl: './card-plant-level.component.html',
  styleUrls: ['./card-plant-level.component.css']
})
export class CardPlantLevelComponent implements OnInit {
  faPlus = faPlus;
  faSearchPlus = faSearchPlus;
  faEye = faEye;
  @Input('options') options;
  @Input('name') name;
  @Input('description') description;
  @Input('data') data;
  @Input('childrenLevel') childrenLevel:{};
  @Input('hasChildren') hasChildren: boolean;
  @Output('onLoadChild') onLoadChild: EventEmitter<CardLevel> = new EventEmitter<CardLevel>();
  @Output('onShowDetails') onShowDetails = new EventEmitter<CardLevel>();
  @Input('collapseSideBar') collapseSideBar: boolean;
  @ViewChild('cardLevelDrag') cardLevelDrag: CdkDrag;
  chartOption: EChartsOption;
  dragPosition: {x:number , y:number }
  cardPositions: [{x:number, y:number}];
  constructor(private configService: ConfigService, private router: Router) { 
    this.cardPositions = configService.configs['positions'];
  }

  ngOnInit(): void {
    this.dragPosition = this.getPosition(this.name);
    this.chartOption = {
      tooltip: {
          trigger: 'item'
      },
      legend: {
          orient:'vertical',
          left:'left',
          itemHeight: 10,
          itemWidth: 10,
          textStyle:{fontSize:10}
      },
      
      color:["#ED7D31", "#E81818", "#70AD47"],
      series: [
          {
              name: "Alarmes",
              type: 'pie',
              radius: ['20%','80%'],
              left: "30%",
              top: "0",
              label: {
                  formatter: '{d}%',
                  fontSize: 11,
                  position:'inside',
                  fontWeight:"bold"
              },
              labelLine:{length: 1},
              emphasis: {
                  label: {
                      show: true,
                      fontSize: '10',
                      fontWeight: 'bold'
                  }
              },
              data: this.data.map(x => {
                var color = '#4472C4';
                if(x['name'] == 'NORMAL') color = '#70AD47';
                else if(x['name'] == 'CRITICO') color = '#E81818';
                else if(x['name'] == 'ALERTA') color = '#fdd835';
                return {
                        name: x['name'], 
                        value: x['value'],
                        itemStyle:{color: color}
                      }
              })
          }
      ]
    };
    
  }

  savePosition(event, id){
    
    var objPositionSaved = JSON.parse(localStorage.getItem(id)) != null ? JSON.parse(localStorage.getItem(id)) : this.cardPositions[id];

    var positionX = event.distance.x;
    var positionY = event.distance.y;
    var x = objPositionSaved != null ? objPositionSaved['x'] + positionX : positionX;
    var y = objPositionSaved != null ? objPositionSaved['y'] + positionY : positionY;

    if(x < 0) x = 0;
    if(y < 0) y = 0;

    this.cardPositions[id] = {x, y};
    localStorage.setItem(id, JSON.stringify({x, y}));
  }

  getPosition(id){
    var objPositionSaved = this.cardPositions[id];
    var objPositionSavedlocal = JSON.parse(localStorage.getItem(id));

    var dragPosition;
    if(objPositionSaved != null){
      dragPosition = {x : objPositionSaved['x'], y: objPositionSaved['y']};
    }
    else{
      dragPosition = {x : 0, y: 0};
      
    }
    
    if(objPositionSavedlocal != null){
      dragPosition = {x : objPositionSavedlocal['x'], y: objPositionSavedlocal['y']};
    }
    
    return dragPosition;
  }
  loadChild(name, description){
    var cardLevel = new CardLevel()
    cardLevel.name = name;
    cardLevel.description = description;
    this.onLoadChild.emit(cardLevel);
  }

  showDetails(){
    var cardLevel = new CardLevel();

    cardLevel.name = this.name;
    cardLevel.chartData = this.data;
    cardLevel.description = this.description;
    this.onShowDetails.emit(cardLevel);
  }

  goToEquipment(name){
    this.router.navigate(['equipment/'+name]);
  }
}
