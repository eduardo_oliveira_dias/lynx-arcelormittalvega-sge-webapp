import { Component, Input, OnInit } from '@angular/core';
import { IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { Indicator } from 'src/app/Core/Models/Indicator';

@Component({
  selector: 'app-card-indicator',
  templateUrl: './card-indicator.component.html',
  styleUrls: ['./card-indicator.component.css']
})
export class CardIndicatorComponent implements OnInit {
  @Input('name') name = '';
  @Input('value') value = '';
  @Input('unit') unit = '';
  @Input('iconDefinition') iconDefinition: IconDefinition;
  @Input('indicator') indicator: Indicator
  @Input('loadingIndicator') loadingIndicator: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
