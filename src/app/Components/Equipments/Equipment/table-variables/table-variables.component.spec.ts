import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableVariablesComponent } from './table-variables.component';

describe('TableVariablesComponent', () => {
  let component: TableVariablesComponent;
  let fixture: ComponentFixture<TableVariablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TableVariablesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableVariablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
