import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TagVariable } from 'src/app/Core/Models/TagVariable';
import { faEdit, faChartLine, faTimes } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-table-variables',
  templateUrl: './table-variables.component.html',
  styleUrls: ['./table-variables.component.css']
})
export class TableVariablesComponent implements OnInit {
  @Input('variables') variables: TagVariable[];
  @Input('variablesInGraphic') variablesInGraphic: string[];
  @Output('onSelectVariable') onSelectVariable = new EventEmitter<TagVariable>()
  @Output('onAddToGraphic') onAddToGraphic = new EventEmitter<TagVariable>()
  @Output('onRemoveFromGraphic') onRemoveFromGraphic = new EventEmitter<TagVariable>()

  faEdit = faEdit;
  faChartLine = faChartLine;
  faTimes = faTimes;

  loading = false;
  cols = [
    {field:'description', header:'Variáveis'}
  ];
  constructor() { }

  ngOnInit(): void {
    
  }

  selectVariable(variable: TagVariable){
    this.onSelectVariable.emit(variable);
    
  }

  addToGraphic(event, variable: TagVariable){
    this.onAddToGraphic.emit(variable);
    
  }

  removeFromGraphic(event, variable: TagVariable){
    this.onRemoveFromGraphic.emit(variable);
  }

  variableIsInGraphic(variableName: string){
    return this.variablesInGraphic.indexOf(variableName) != -1;
  }

}
