import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Equipment } from 'src/app/Core/Models/Equipment';
import { EquipmentService } from 'src/app/Core/Services/equipment.service';
import { faCog, faBars, faTimes, faEdit, faCalendarAlt, faSearch, faImages, faSave, faFileMedicalAlt, faChartBar } from '@fortawesome/free-solid-svg-icons';
import { TagVariable } from 'src/app/Core/Models/TagVariable';
import { DateService } from 'src/app/Core/Services/date.service';
import { GraphicTrend } from 'src/app/Core/Models/Graphic/GraphicTrend';
import { GraphicTrendRequest } from 'src/app/Core/Models/Request/GraphicTrendRequest';
import { GraphicService } from 'src/app/Core/Services/graphic.service';
import { EChartsOption, graphic } from 'echarts';
import { ptBR } from 'src/app/Core/Models/util/dateOptions' 
import { FileService } from 'src/app/Core/Services/file.service';
import { FeedbackService } from 'src/app/Core/Services/feedback.service';
import { VariableParameterRequest } from 'src/app/Core/Models/Request/VariableParameterRequest';
import { VariableService } from 'src/app/Core/Services/variable.service';
import { IndicatorService } from 'src/app/Core/Services/indicator.service';
import { Indicator } from 'src/app/Core/Models/Indicator';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-equipment',
  templateUrl: './equipment.component.html',
  styleUrls: ['./equipment.component.css']
})
export class EquipmentComponent implements OnInit {
  equipment: Equipment = new Equipment();
  code: string;
  loadingEquipment: boolean;
  faBars = faBars;
  faCog = faCog;
  faTimes = faTimes;
  faEdit = faEdit;
  faCalendarAlt = faCalendarAlt;
  faSearch = faSearch;
  faImages = faImages;
  faSave = faSave;
  faFileMedicalAlt = faFileMedicalAlt;
  faChartBar = faChartBar;
  dateOptions = ptBR;
  loadingGraphic = false;
  loadingImage = false;
  fileBkg = null;
  styleBackGround = {};
  cols = [
    {field:'description', header:'Variáveis'}
  ];
  colsParameters = [
    { field: 'value', name: 'Valor Atual' },
    { field: 'engUnits', name: 'Unidade' },
    { field: 'iP_High_High_Limit', name: 'Limite Critico Superior' },
    { field: 'iP_High_Limit', name: 'Limite Alerta Superior' },
    { field: 'iP_Low_Low_Limit', name: 'Limite Critico Inferior' },
    { field: 'iP_Low_Limit', name: 'Limite Alerta Inferior' },
    { field: 'iP_Graph_Maximum', name: 'Escala Gráfico Máximo' },
    { field: 'iP_Graph_Minimum', name: 'Escala Gráfico Inferior' },
  ]
  selectedVariable: TagVariable;
  variablesInGraphic: string[] = [];
  option: EChartsOption = {
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: [],
        top: 'bottom'
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: []
    },
    yAxis: {
        type: 'value',
        min:0,
        max: 10,
    },
    series: []
  }
  graphicTrend: GraphicTrend[] = [];
  startTime: Date;
  endTime: Date;
  imageFile: File;
  displayModal = false;
  displayModalEdit = false;
  savingImage = false;
  savingParameter = false;
  parameterRequest =  new VariableParameterRequest();
  navigationSubscription;
  kpi = new Indicator();
  formGraphic: FormGroup;
  formEditParameter: FormGroup;
  loadingKPI = false;
  ipGraphMax = 10;
  ipGraphMin = 0;
  constructor(private route: ActivatedRoute, 
              private equipmentService: EquipmentService, 
              private graphicService: GraphicService,
              private dateService: DateService,
              private fileService: FileService,
              private feedbackService: FeedbackService,
              private variableService: VariableService,
              private router: Router,
              private indicatorService: IndicatorService) {

    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      if (e instanceof NavigationEnd) {
        var lastCode = this.code;
        this.code = this.route.snapshot.paramMap.get('code');
        if(lastCode != this.code){
          this.loadEquipment();
        }
      }
    });
  }

  ngOnInit(): void {
    
  }

  loadEquipment(){
    this.selectedVariable= null;
    this.variablesInGraphic = [];
    this.option = {
      tooltip: {
          trigger: 'axis'
      },
      legend: {
          data: [],
          top: 'bottom'
      },
      xAxis: {
          type: 'category',
          boundaryGap: false,
          data: []
      },
      yAxis: {
          type: 'value',
          min: this.ipGraphMin,
          max: this.ipGraphMax
      },
      series: []
    }
    this.graphicTrend = [];
    this.imageFile = null;
    this.displayModal = false;
    this.displayModalEdit = false;
    this.savingImage = false;
    this.savingParameter = false;
    this.kpi = new Indicator();
    this.parameterRequest =  new VariableParameterRequest();
    this.endTime = new Date();
    this.startTime = this.dateService.add('days',this.endTime, -1);

    this.formGraphic = new FormGroup({
      startTime: new FormControl(this.startTime, [Validators.required]),
      endTime: new FormControl(this.endTime, [Validators.required])
    });

    this.formEditParameter = new FormGroup({
      variable: new FormControl(this.VariableName, [Validators.required]),
      parameter: new FormControl(this.parameterRequest.name, [Validators.required]),
      parameterValue: new FormControl(this.parameterRequest.value),
      parameterNewValue: new FormControl(this.parameterRequest.newValue, [Validators.required, Validators.pattern(/^-?[0-9]+([.]{0,1}[0-9]+)?$/)]),
    })
    this.loadingEquipment = true
    this.equipmentService.getEquipment(this.code)
      .then(res => {
        this.equipment = res;
        this.loadKPI();
        this.loadBackground();
        if(this.equipment.variables != undefined && this.equipment.variables.length > 0){
          this.addToGraphic(this.equipment.variables[0]);
        }
      })
      .catch(err => {
        this.feedbackService.error(err.error);
      })
      .finally(() => {
        this.loadingEquipment = false;
      })
  }

  selectVariable(variable){
    this.selectedVariable = variable;
    this.ipGraphMax = variable.iP_Graph_Maximum;
    this.ipGraphMin = variable.iP_Graph_Minimum;

  }

  addToGraphic(variable){
    if(this.formGraphic.valid){
      this.selectVariable(variable);
      var graphicTrendRequest = new GraphicTrendRequest();
      graphicTrendRequest.variableNames = [variable.name];
      graphicTrendRequest.endTime = this.endTime;
      graphicTrendRequest.startTime = this.startTime;
  
      this.loadingGraphic = true;
      this.graphicService.getTrendAlarms(graphicTrendRequest)
        .then(res => {
          if(res.length < 1){
            this.feedbackService.warn("Dados insuficientes");
            this.variablesInGraphic = [variable.name];
            var graphicEmpty = new GraphicTrend();
            graphicEmpty.info = variable.description;
            graphicEmpty.description = variable.description;
            graphicEmpty.items = [];
            this.graphicTrend = [graphicEmpty]
            this.setDataToGraphic(this.graphicTrend[0]);
          }else{
            this.variablesInGraphic = [variable.name];
            this.graphicTrend = res;
            this.setDataToGraphic(this.graphicTrend[0]);
          }
        }).catch(err =>{
          console.log(err);
          this.feedbackService.error(err.error);
          
        }).finally(() => {
          this.loadingGraphic = false;
        })
    }
  }

  setDataToGraphic(data: GraphicTrend){
    var limits = this.getLimitMarklines();
    var description = data != undefined ? data.description : '';
    this.option = {
      tooltip: {
          trigger: 'axis'
      },
      legend: {
          data: [description],
          top: 'bottom',
          textStyle: {fontStyle:  'italic'}
      },
      xAxis: {
          type: 'category',
          boundaryGap: false,
          min:this.startTime.toLocaleString(),
          max: this.endTime.toLocaleString(),
          data: data != undefined ? data.items.map(i => i.name) : []
      },
      yAxis: {
          type: 'value',
          min: this.ipGraphMin,
          max: this.ipGraphMax
      },
      series: {
            name: data != undefined ? data.info : '',
            data: data != undefined ? data.items.map(i => <number>i.value.toFixed(3)) : [],
            type:'line',
            smooth: false,
            markLine:{ 
              data: limits
            }
          }
    }
  }
  

  searchInGraphic(){
    if(this.variablesInGraphic.length > 0){
      var graphicTrendRequest = new GraphicTrendRequest();
      graphicTrendRequest.variableNames = this.variablesInGraphic;
      graphicTrendRequest.endTime = this.endTime;
      graphicTrendRequest.startTime = this.startTime;
  
      this.loadingGraphic = true;
      this.graphicService.getTrendAlarms(graphicTrendRequest)
        .then(res => {
          if(res.length < 1){
            this.feedbackService.warn("Dados insuficientes");
          }else{
            this.setDataToGraphic(res[0]);
          }
        }).catch(err=>{
          console.log(err);
          this.feedbackService.error(err.error);

        }).finally(() => {
          this.loadingGraphic = false;
        })
    }
  }

  loadBackground(){
    this.loadingImage = true;
    this.fileService.getImage(this.equipment.name)
      .then(res =>{
        this.loadingImage = false;
        var image ="";
        if(res.type == 'jpg') image = 'data:image/jpeg;base64,'+res.content;
        if(res.type == 'png') image = 'data:image/png;base64,'+res.content;

        this.fileBkg = res['image'];
        this.styleBackGround = {'background-image': 'url('+image+')', 'background-repeat':'no-repeat', 'background-size':'cover','background-position':'center'};

      })
      .catch(err=>{
        this.loadingImage = false;
        this.feedbackService.warn(err.error);

      })
  }

  showFormImage(){
    this.imageFile = null;
    this.displayModal = true;
    
  }

  onChange(event){
    var file = event.srcElement.files[0];
    if(file != null){
      this.imageFile = file; 
  
      document.getElementById('image-file-label').innerHTML = file.name;
    }
  }

  resetInput(){
    
    try{
      var inputFile = document.getElementById('image-file-label');
      if(inputFile) inputFile.innerHTML = 'Escolher arquivo';

    }catch(e){
      console.log(e);
    }
  }

  saveFile(){
    if(this.imageFile != null){
      this.savingImage = true;
      this.fileService.uploadImage(this.imageFile,this.equipment.name)
        .then(res => {
          this.displayModal = false;
          this.savingImage = false;
          this.feedbackService.success(res['body'].message);
          this.loadBackground();
        }).catch(err =>{
          this.feedbackService.error(err.error);
          this.savingImage = false;
        });
    }
  }

  saveParameter(){
    this.savingParameter = true;
    this.variableService.updateParameter(this.parameterRequest)
      .then(res => {
        this.displayModalEdit = false;
        this.feedbackService.success(res['message']);
        this.selectedVariable[this.parameterRequest.field] = this.parameterRequest.newValue;
        this.addToGraphic(this.selectedVariable);
      })
      .catch(err =>{
        this.feedbackService.error(err.error);
        
      })
      .finally(() => {
        this.savingParameter = false;
      })
  }

  showModalEdit(parameter){

    var parameterSelected = this.colsParameters.find(x => x.field == parameter);
    this.parameterRequest.field = parameterSelected.field;
    this.parameterRequest.name = parameterSelected.name;
    this.parameterRequest.variable = this.selectedVariable;
    this.parameterRequest.value = this.selectedVariable[parameterSelected.field];
    this.parameterRequest.newValue = null;
    this.displayModalEdit = true;
  }
  
  loadKPI(){
    this.loadingKPI = true;
    this.indicatorService.getKPI(this.equipment.name)
    .then(res => {
      this.kpi = res;
    })
    .catch(err => {
      this.feedbackService.error(err.error);
    })
    .finally(()=>{
      this.loadingKPI = false;
    })
  }
  
  removeVariableFromGraphic(variable){
    if(variable != undefined){
      this.graphicTrend = this.graphicTrend.filter(x => x.info != variable.description);
      var variableSelected = this.equipment.variables.find(x => x.name == variable.name);
      if(variableSelected != undefined && variableSelected != null){
        this.variablesInGraphic = this.variablesInGraphic.filter(x => x != variableSelected.name);
        
        if(this.graphicTrend.length > 0) this.setDataToGraphic(this.graphicTrend[0]);
        else this.resetGraphic();
      }
    }
  }

  resetGraphic(){
    this.option = {
      tooltip: {
          trigger: 'axis'
      },
      legend: {
          data: [],
          top: 'bottom'
      },
      xAxis: {
          type: 'category',
          boundaryGap: false,
          data: []
      },
      yAxis: {
          type: 'value',
          min:0,
          max: 10,
      },
      series: []
    }
  }

  getLimitMarklines(){
    var keys = Object.keys(this.selectedVariable);
    var markLines = [];
    for(var key of keys){
      if(key == 'iP_High_High_Limit' || key == 'iP_High_Limit' || key == 'iP_Low_Low_Limit' || key == 'iP_Low_Limit'){
        var color = (key == 'iP_High_High_Limit' || key == 'iP_Low_Low_Limit') ? '#dc3545' : '#ffc107';
        var label = '';
        switch(key){
          case 'iP_High_High_Limit': 
            label = 'Crítico Sup.';
            break;
          case 'iP_High_Limit': 
            label = 'Alerta Sup.';
            break;
          case 'iP_Low_Low_Limit': 
            label = 'Crítico Inf.';
            break;
          case 'iP_Low_Limit': 
            label = 'Alerta Inf.';
            break;
        }
        var markLine = {
          name: this.colsParameters.find(x => x.field == key).name,
          yAxis: this.selectedVariable[key],
          label: {
              formatter: label,
              position: 'start',
              distance: [20,8],
          },
          lineStyle:{color:color}
        }
        markLines.push(markLine);

      }
    }

    return markLines.filter(x => x.yAxis != null);
  }


  get VariableName(){
    return this.selectedVariable?.description
  }
  get startTimeForm(){
    return this.formGraphic.get('startTime');
  }
  get endTimeForm(){
    return this.formGraphic.get('endTime');
  }

  get parameterNewValueForm(){
    return this.formEditParameter.get('parameterNewValue');
  }
}
